import { light as lightTheme, mapping } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import React from 'react';
import { ApplicationProvider, IconRegistry } from 'react-native-ui-kitten';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { AppContainer } from "./src/navigation";
import { persistor, store } from "./src/reducers";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

React.Component.prototype._setStateAsync = function (state) {
  return new Promise((resolve) => { this.setState(state, resolve) });
}

React.PureComponent.prototype._setStateAsync = function (state) {
  return new Promise((resolve) => { this.setState(state, resolve) });
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider
          mapping={mapping}
          theme={lightTheme}
        >
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <AppContainer />
            </PersistGate>
          </Provider>
        </ApplicationProvider>
      </>
    )
  }
}

export default App;
