import React, { PureComponent } from 'react'
import { Picker, StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 8
  },

  containerLabel: {
    flexDirection: "row",
    alignItems: "center"
  },

  labelError: {
    color: "red",
    fontSize: 12
  },

  label: {
    padding: 2,
    paddingRight: 16
  },

  input: {
    backgroundColor: "#fff",
    borderRadius: 8,
    overflow: 'hidden'
  },

  error: {
    borderWidth: 0.5,
    borderColor: "red"
  },

  disabled: {
    backgroundColor: "#E6E6E6"
  }
})

class MyPicker extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      errorMessage: ""
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { checkValidForm, rules, name, selectedValue} = nextProps

    if(checkValidForm) {
      const errorMessage = this._validateByRule(rules, name, selectedValue)
      this.setState({ errorMessage})
    }
  }

  componentDidMount() {
    const { rules, name, selectedValue } = this.props
    this._validateByRule(rules, name, selectedValue)
  }

  _validateByRule = (rules, name, value) => {
    if(rules) {
      for(let rule of rules) {
        const message = rule(value)
        if(typeof message === "string") {
          this.props.onValidate({
            name,
            valid: false
          })
          return message
        }
      }
    }

    this.props.onValidate({
      name,
      valid: true
    })
    return null
  }

  _renderLabel = () => {
    const { label } = this.props
    if(!label) return null

    return (
      <View>
        <Text style={styles.label}> { label } </Text>
      </View>
    )
  }

  _renderMessageError = () => {
    const { errorMessage } = this.state

    return (
      <View style={{height: 16, paddingLeft: 4}}>
        <Text style={styles.labelError}>
          { errorMessage }
        </Text>
      </View>
    )
  }

  _renderPicker = () => {
    const { renderItem, selectedValue, onValueChange, disabled } = this.props
    const { errorMessage } = this.state

    return (
      <View
        style={[styles.input, (errorMessage && styles.error), (disabled && styles.disabled)]}
        pointerEvents={disabled ? "none" : "auto"}
      >
        <Picker
          selectedValue={selectedValue}
          onValueChange={(itemValue, itemIndex) => onValueChange(itemValue, itemIndex)}
        >
          <Picker.Item label={"Vui lòng chọn"} value={""} />
          { renderItem() }
        </Picker>
      </View>
    )
  }

  render() {
    return (
      <View>
        { this._renderLabel() }
        { this._renderPicker() }
        { this._renderMessageError() }
      </View>
    )
  }
}

MyPicker.defaultProps = {
  label: "",
  name: "",
  selectedValue: "",
  placeholder: "",
  type: "",
  error: "",
  checkValidForm: false,
  disabled: false
}

export { MyPicker }

