import AsyncStorage from '@react-native-community/async-storage';
import * as actions from 'actions/index';
import { persistReducer } from 'redux-persist';

const persistConfig = {
  key: 'auth',
  storage: AsyncStorage
}

const defaultState = {
  isLoading: false,
  error: "",
  accessToken: "",
  userInfo: ""
}

function authReducerPersist(state = defaultState, action) {
  const nextState = { ...state }
  const { payload, type } = action

  switch (type) {
    case actions.GET_ACCESS_TOKEN: {
      nextState.isLoading = true

      return nextState
    }

    case actions.GET_ACCESS_TOKEN_SUCCESS: {
      nextState.isLoading = false
      nextState.accessToken = payload.data.access_token

      return nextState
    }

    case actions.GET_ACCESS_TOKEN_FAIL: {
      nextState.isLoading = false
      nextState.error = `GET_ACCESS_TOKEN_FAIL: ${payload.error}`

      return nextState
    }

    case actions.GET_USER_INFO: {
      nextState.isLoading = true
      return nextState
    }

    case actions.GET_USER_INFO_SUCCESS: {
      nextState.isLoading = false
      nextState.userInfo = payload.data

      return nextState
    }

    case actions.GET_USER_INFO_FAIL: {
      nextState.isLoading = false
      nextState.error = `GET_USER_INFO_FAIL: ${payload.error}`

      return nextState
    }

    case actions.ADD_USER_INFO: {
      nextState.isLoading = true
      return nextState
    }

    case actions.ADD_USER_INFO_SUCCESS: {
      nextState.isLoading = false
      nextState.userInfo = payload.data

      return nextState
    }

    case actions.ADD_USER_INFO_FAIL: {
      nextState.isLoading = false
      nextState.error = `ADD_USER_INFO_FAIL: ${payload.error}`

      return nextState
    }

    case actions.REMOVE_USER_INFO: {
      nextState.isLoading = true

      return nextState
    }

    case actions.REMOVE_USER_INFO_SUCCESS: {
      nextState.isLoading = false
      nextState.userInfo = payload.data

      return nextState
    }

    case actions.REMOVE_USER_INFO_FAIL: {
      nextState.isLoading = false
      nextState.error = `REMOVE_USER_INFO_FAIL: ${payload.error}`

      return nextState;
    }

    default: {
      return state;
    }

  }
}

const authReducer = persistReducer(persistConfig, authReducerPersist)
export { authReducer };
