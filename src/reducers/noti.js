// constanst
import {
    SHIPPER_OUT_OF_MONEY_PUSH_NOTI,
    CHANGE_STATUS_CHOOSE_ORDER_PUSH_NOTI,
    CHANGE_STATUS_CONFRIM_ORDER_PUSH_NOTI,
    CHANGE_STATUS_CANCEL_ORDER_PUSH_NOTI
} from '../constanst';

const initState = {
    valueCheckPushNoti: 0
};

export function notiReducer(state = initState, action) {
    let newState = { ...state };

    if (action.type === CHANGE_STATUS_CANCEL_ORDER_PUSH_NOTI) {
        newState = {
            ...state,
            valueCheckPushNoti: action.payload.valueCheckPushNoti
        }
        return newState;
    } else if (action.type === CHANGE_STATUS_CHOOSE_ORDER_PUSH_NOTI) {
        newState = {
            ...state,
            valueCheckPushNoti: action.payload.valueCheckPushNoti
        }
        return newState;
    } else if (action.type === CHANGE_STATUS_CONFRIM_ORDER_PUSH_NOTI) {
        newState = {
            ...state,
            valueCheckPushNoti: action.payload.valueCheckPushNoti
        }
        return newState;
    } else if (action.type === SHIPPER_OUT_OF_MONEY_PUSH_NOTI) {
        newState = {
            ...state,
            valueCheckPushNoti: action.payload.valueCheckPushNoti
        }
        return newState;
    } else {
        return newState;
    }
}