import { baseURL } from "./services";

export const getDeviceId = ({ authorization, data }) => {
    return {
        method: 'POST',
        url: baseURL + '/device/create',
        body: data,
        authorization
    }
}