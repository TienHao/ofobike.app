import { baseURL } from "./services";

function mapProperty(step, data) {
  switch (step) {
    case 1: {
      return {
        customer_id: data.memberId && Number(data.memberId) || 8,
        sender_fullname: data.senderName,
        sender_phone: data.senderPhone,
        sender_email: data.senderEmail,
        sender_date: data.senderDate,
        sender_time: data.senderTime,
        address_from: data.senderAddress,
        receiver_fullname: data.receiverName,
        receiver_phone: data.receiverPhone,
        receiver_email: data.receiverEmail,
        address_to: data.receiverAddress,
        receiver_date: data.receiverDate,
        receiver_time: data.receiverTime
      };
    }
    case 2: {
      return {
        product_infor: data.packageName,
        mass: Number(data.weights),
        note: data.note,
        descriptions: data.descriptions,
        image: data.packageImage,
        result: data.result
      };
    }
    default: {
      return {};
    }
  }
}

export const submitStep = ({ step, data, authorization }) => {
  let mapData = {};

  if (step === 3) {
    mapData = [mapProperty(1, data.step1), mapProperty(2, data.step2)];
  }

  else if (step === 4) {
    let dataStep4 = [mapProperty(1, data.step1), { ...mapProperty(2, data.step2), result: data.step3.result }];

    dataStep4.forEach(function (value) {
      delete value.sender_email
      delete value.receiver_email
      delete value.cod_money
      delete value.descriptions
      delete value.result
    })

    dataStep4[0].member_id = 0;
    dataStep4[1].money_fee = "0.000";

    mapData = dataStep4;
  }
  else {
    mapData = mapProperty(step, data);
  }

  return {
    method: 'POST',
    url: baseURL + `/delivery/submit/${step}`,
    body: mapData,
    authorization
  }
}

export const getDeliveryFee = ({ step1, step2, authorization }) => {
  return {
    method: 'POST',
    url: baseURL + `/delivery/fee`,
    authorization,
    body: [mapProperty(1, step1), mapProperty(2, step2)]
  }
}

export const getApiHistories = ({ authorization, userId, typeId }) => {
  return {
    method: 'POST',
    url: baseURL + `/delivery/history`,
    authorization,
    body: {
      id: userId,
      type_id: typeId
    }
  }
}

export const getApiHistoryTree = ({ authorization, code }) => {
  return {
    method: 'POST',
    url: baseURL + `/delivery/detail`,
    authorization,
    body: { code }
  }
}

export const getApiServices = ({ authorization }) => {
  return {
    method: 'GET',
    url: baseURL + `/delivery/services`,
    authorization
  }
}

export const getApiCancelOrder = ({ authorization, data }) => {
  return {
    method: 'POST',
    url: baseURL + `/delivery/cancel`,
    authorization,
    body: data
  }
}

export const getApiDeliveryChooseShipper = ({ authorization }) => {
  return {
    method: "GET",
    url: baseURL + `/delivery/choose`,
    authorization
  }
}

export const getApiChooseOrder = ({ authorization, data }) => {
  return {
    method: "POST",
    url: baseURL + `/delivery/choose`,
    authorization,
    body: data
  }
}

export const updateStatusDelivery = ({ authorization, data }) => {
  return {
    method: "POST",
    url: baseURL + '/delivery/updatestatus',
    authorization,
    body: data
  }
}

export const getInfoShipper = ({ authorization, data }) => {
  return {
    method: "POST",
    url: baseURL + '/member/detail',
    authorization,
    body: data
  }
}

export const postRateComment = ({ authorization, data }) => {
  return {
    method: "POST",
    url: baseURL + '/delivery/updatenote',
    authorization,
    body: data
  }
}
