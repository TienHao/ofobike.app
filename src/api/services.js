import axios from 'axios';

const PROTOCOL = "http";
const HOST = "motobike.vn";
const PORT = "";
const PREFIX = "/api";

export const base = `${PROTOCOL}://${HOST}`;
export const baseURL = `${PROTOCOL}://${HOST}${PORT}${PREFIX}`;

export const defaultSecret = {
  client_id: 1,
  client_secret: "27f0db66dcaccab1a53ed8e9d2f9a183"
}

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (error.response.data) {
    return Promise.resolve(error.response);
  }

  return Promise.reject(error);
});

export const callApi = function ({ method, url, authorization, urlParams, body }, debug = false) {
  url = (urlParams && url + urlParams) || url;

  return axios({
    method,
    url,
    headers: {
      "authorization": authorization || "",
      "Accept": "application/json",
      "Content-Type": "application/json",
      "X-MOBILE-APP": "IsAppRequest"
    },
    data: body
  })
    .then(response => response.data)
    .catch(error => console.log({ error }))
}
