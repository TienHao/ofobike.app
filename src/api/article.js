import { baseURL } from "./services";

export const getApiArticles = ({ page, authorization}) => {
  return {
    method: 'GET',
    url: baseURL + `/article?page=${page}&cate_id=1`,
    authorization
  }
}

export const getApiArticleById = ({ id, authorization}) => {
  return {
    method: 'GET',
    url: baseURL +`/article/id?id=${id}`,
    authorization
  }
}
