import { colors } from 'config/colors';
import React, { Fragment, PureComponent } from 'react';
import { Keyboard, StyleSheet } from 'react-native';
import { Icon } from 'react-native-eva-icons';
import { FloatingAction } from "react-native-floating-action";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const styles = StyleSheet.create({
  shadowStyle: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 3.84,
    elevation: 5,
  }
});

const actions = [
  {
    position: 1,
    text: "Tạo vận đơn",
    name: "btn_order",
    color: "#9b59b6",
    icon: <Icon name="car-outline" width={32} height={32} fill={'#fff'} />
  },
  {
    position: 2,
    text: "Lịch sử",
    name: "btn_history",
    color: "#97E8C8",
    icon: <Icon name="shopping-bag-outline" width={32} height={32} fill={'#fff'} />
  }
];

class FloatButtonContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.state = this._getInitialState()

    this.keyboardWillShow = this.keyboardWillShow.bind(this)
    this.keyboardWillHide = this.keyboardWillHide.bind(this)
  }

  _getInitialState = () => {
    return {
      active: false,
      isVisible: true
    }
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }

  keyboardWillShow = event => {
    this.setState({
      isVisible: false
    })
  }

  keyboardWillHide = event => {
    this.setState({
      isVisible: true
    })
  }

  _navigate = (iconName) => {
    if (iconName === "btn_order") this.props.navigation.navigate('Order')
    if (iconName === "btn_history") this.props.navigation.navigate('History')
  }

  render() {
    const { isVisible } = this.state
    return (
      <Fragment>
        {
          isVisible && this.props.active
          &&
          <FloatingAction
            floatingIcon={<Icon name="edit-outline" width={32} height={32} fill={'#fff'} />}
            color={colors.primary.accent}
            actions={actions}
            onPressItem={name => this._navigate(name)}
          />
          || null
        }
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const FloatButtonComp = connect(mapStateToProps, mapDispatchToProps)(FloatButtonContainer);
export { FloatButtonComp };

