import { colors } from 'config/colors'
import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { getHeightPercentage } from 'utils'

export const Loading = function() {
  return(
    <View style={{height: getHeightPercentage(25), justifyContent: "center", alignItems: "center", flex: 1}}>
      <ActivityIndicator size="large" color={colors.primary.default} />
    </View>
  )
}
