import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: "transparent"
    },
    label: {
        fontSize: 12,
        fontFamily: "roboto-regular",
        textAlign: "left"
    },
    placeHolder: {
        color: "#000",
        alignSelf: "stretch",
        borderColor: "#D9D5DC",
        borderBottomWidth: 1,
        fontFamily: "roboto-regular",
    },
    helper1: {
        fontSize: 12,
        fontFamily: "roboto-regular",
        textAlign: "left"
    }
});