// libary
import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Platform, Text } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

class OfoDateTimePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDateTimeValue: null,
            isStartingDateTimePickerVisible: false,
            dateOrTimeValue: null,
            datePickerVisible: false,
            timePickerVisible: false
        }
    }

    saveStartingDateTime = (value) => {
        this.setState({
            startDateTimeValue: value
        });

        this.props.getDateTime({
            dateTime: value,
            checkGetDateTime: this.props.checkGetDateTime
        });
    }

    onChangeDateTimePickerIOS = (event, value, visibilityVariableName, saveValueFunctionName) => {
        this.setState({
            dateOrTimeValue: value,
            [visibilityVariableName]: Platform.OS === 'ios' ? true : false
        });

        if (event.type === 'set') {
            saveValueFunctionName(value);
        }
    }

    onChangeDateTimePickerAndroid = (event, value, dateTimePickerMode, visibilityVariableName, saveValueFunctionName) => {
        this.setState({
            dateOrTimeValue: value,
            datePickerVisible: false,
        });

        if (event.type === 'set' && dateTimePickerMode === 'datetime') {
            this.setState({
                timePickerVisible: true,
            })
        } else if (event.type === 'set' && dateTimePickerMode === 'date') {
            this.setState({
                [visibilityVariableName]: Platform.OS === 'ios' ? true : false
            })

            saveValueFunctionName(value);
        }
    }

    onChangeTimeAndroid = (event, value, dateTimePickerMode, visibilityVariableName, saveValueFunctionName) => {
        let newDateTime = value;

        if (event.type === 'set' && dateTimePickerMode === 'datetime') {
            newDateTime = this.state.dateOrTimeValue;

            const newHours = value.getHours();
            const newMinutes = value.getMinutes();

            newDateTime.setHours(newHours);
            newDateTime.setMinutes(newMinutes);
            newDateTime.setSeconds(0);
        }

        this.setState({
            dateOrTimeValue: newDateTime,
            datePickerVisible: false,
            timePickerVisible: false,
            [visibilityVariableName]: Platform.OS === 'ios' ? true : false
        });

        if (event.type === 'set') {
            saveValueFunctionName(newDateTime);

            this.props.getDateTime({
                dateTime: value,
                checkGetDateTime: this.props.checkGetDateTime
            });
        }
    }

    renderDateTimePicker = (dateTimePickerVisible, visibilityVariableName, dateTimePickerMode, defaultValue, saveValueFunctionName) => {
        return (
            <View>
                {Platform.OS === 'ios' && dateTimePickerVisible && (
                    <DateTimePicker
                        minimumDate={new Date(Date.now())}
                        mode={dateTimePickerMode}
                        value={defaultValue}
                        onChange={(event, value) => this.onChangeDateTimePickerIOS(event, value, visibilityVariableName, saveValueFunctionName)}
                    />
                )}
                {
                    Platform.OS === 'android' && dateTimePickerVisible && this.state.datePickerVisible &&
                    (
                        <DateTimePicker
                            minimumDate={new Date(Date.now())}
                            mode={"date"}
                            display="default"
                            value={defaultValue}
                            onChange={(event, value) => this.onChangeDateTimePickerAndroid(event, value, dateTimePickerMode, visibilityVariableName, saveValueFunctionName)}
                        />
                    )
                }
                {
                    Platform.OS === 'android' && dateTimePickerVisible && this.state.timePickerVisible &&
                    (
                        <DateTimePicker
                            minimumDate={new Date(Date.now())}
                            mode={"time"}
                            display={"spinner"}
                            is24Hour={false}
                            value={defaultValue}
                            onChange={(event, value) => this.onChangeTimeAndroid(event, value, dateTimePickerMode, visibilityVariableName, saveValueFunctionName)}

                        />
                    )
                }
            </View>
        )
    }

    formatDatetime = (date) => {
        if (date === null) {
            return null;
        }

        let forMatDatetime = moment(date).format('DD/MM/YYYY- hh:mm:ss');

        return forMatDatetime;
    }

    renderDatePicker = (mode, visibilityVariableName) => {
        switch (mode) {
            case "datetime":
                return this.setState({
                    [visibilityVariableName]: true,
                    datePickerVisible: true,
                    timePickerVisible: false
                });
        }
    }

    render() {
        let defaultShiftStartDateTime = new Date();

        defaultShiftStartDateTime.setDate(defaultShiftStartDateTime.getDate());
        defaultShiftStartDateTime.setHours(9);
        defaultShiftStartDateTime.setMinutes(0);
        defaultShiftStartDateTime.setSeconds(0);

        return (
            <View style={{ marginBottom: 15 }}>
                <View>
                    <Text style={{ color: 'grey', fontSize: 13 }}>{this.props.labelDateTime}</Text>
                </View>
                <TouchableOpacity
                    style={{ borderBottomWidth: 1, borderColor: 'grey', flexDirection: 'row' }}
                    onPress={() => {
                        this.renderDatePicker('datetime', "isStartingDateTimePickerVisible")
                    }}
                >
                    <View style={{ flex: 1 }}>
                        <TextInput
                            style={{ color: 'black' }}
                            placeholder={this.props.placeholderDateTime}
                            editable={false}
                            value={this.formatDatetime(this.state.startDateTimeValue)}
                        />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                        <IconAntDesign style={{ color: 'grey', marginRight: 10, fontSize: 14 }} name="caretdown" />
                    </View>
                </TouchableOpacity>
                {
                    this.renderDateTimePicker(
                        this.state.isStartingDateTimePickerVisible,
                        "isStartingDateTimePickerVisible",
                        "datetime",
                        defaultShiftStartDateTime,
                        this.saveStartingDateTime
                    )
                }
            </View>
        );
    }
}

export default OfoDateTimePicker;