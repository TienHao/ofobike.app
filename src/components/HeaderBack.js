// libary
import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


const styles = StyleSheet.create({
  container: {
    textAlign: "center"
  }
})

class HeaderBackContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.state = this._getInitialState();
  }

  _getInitialState = () => {
    return {}
  }

  _back = () => {
    if (this.props.navigationOptions && this.props.navigationOptions.handleBackPress) {
      const { handleBackPress } = this.props.navigationOptions
      const screen = new handleBackPress()
      screen._handleBackPress(this.props.navigation)
    } else {
      this.props.navigation.dismiss() || this.props.navigation.popToTop()
    }
  }

  render() {
    return (
      <TouchableOpacity
        style={[{ ...this.props.style, marginLeft: 5 }]}
        onPress={() => this._back()}
      >
        <View>
          <Text>
            <MaterialCommunityIcons name='arrow-left' style={{ fontSize: 24 }} />
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const HeaderBackComp = connect(mapStateToProps, mapDispatchToProps)(HeaderBackContainer);
export { HeaderBackComp };

