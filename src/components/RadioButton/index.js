// libary
import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

// styles
import { styles } from './styles';

class RadioButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={[styles.radioButton, { marginRight: 10, marginTop: 10 }]}>
                <View style={[styles.radioButtonGroup, { height: this.props.radioButton.size, width: this.props.radioButton.size, borderColor: this.props.radioButton.color }]}>
                    {
                        (this.props.radioButton.isSelected) ? (
                            <View style={[styles.radioIcon, { height: this.props.radioButton.size / 2, width: this.props.radioButton.size / 2, backgroundColor: this.props.radioButton.color }]}></View>
                        ) : null
                    }
                </View>
                <Text style={{ marginLeft: 5 }}>{this.props.radioButton.label}</Text>
            </TouchableOpacity>
        );
    }
}

export default RadioButton;