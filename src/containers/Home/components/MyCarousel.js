import { base } from 'api';
import { colors } from 'config/colors';
import React, { Fragment } from 'react';
import { Image, View } from "react-native";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { getHeightPercentage, widthDevice } from "utils";

export const MyCarousel = function (props) {
  function _renderItem({ item, index }) {
    return (
      <Image
        style={{ flex: 1, height: 120 }}
        source={{ uri: `${base}/${item.Image}` }}
      />
    );
  }

  return (
    <Fragment>
      {
        <View>
          <Carousel
            contentContainerCustomStyle={{ paddingTop: 10 }}
            data={props.sliders}
            renderItem={_renderItem}
            sliderWidth={widthDevice}
            itemWidth={widthDevice - 20}
            onSnapToItem={(index) => props.onSnapToItem(index)}
          />
          <Pagination
            dotsLength={props.sliders.length}
            activeDotIndex={props.activeSlide}
            containerStyle={{ backgroundColor: 'transparent', paddingVertical: 10 }}
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 1,
              backgroundColor: colors.primary.default
            }}
            inactiveDotStyle={{
              borderColor: colors.primary.default,
              borderWidth: 2,
              backgroundColor: "transparent"
            }}
            inactiveDotOpacity={0.6}
            inactiveDotScale={0.5}
          />
        </View>
      }
    </Fragment>
  )
}
