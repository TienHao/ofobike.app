import { base } from 'api';
import React from 'react';
import { Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import { getWidthPercentage } from 'utils/index';

const styles = StyleSheet.create({
  containerImage: {
    flex: 1,
    height: 150,
    overflow: "hidden"
  },

  item: {
    flexBasis: getWidthPercentage(45),
    backgroundColor: '#fff',
    margin: 8
  },

  shadowStyle: {
    shadowColor: "#fefefe",
    shadowOffset: {
      width: 50,
      height: 100,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 3,
  }
})

export const ArticleItem = (props) => {
  const { item, index } = props.article

  return (
    <TouchableWithoutFeedback delayPressIn={50} onPress={() => props.viewArticleDetail(item)}>
      <View>
        {/* {
          index === 0 &&
          <View style={{ paddingHorizontal: 16 }}>
            <Text style={{ fontSize: 22, fontWeight: "600" }}>Tin tức</Text>
          </View>
        } */}
        <View style={styles.item}>
          <View style={styles.containerImage}>
            <Image
              style={{ flex: 1 }}
              source={{ uri: `${base}/${item.image}` }}
            />
          </View>
          <View style={{ height: 150, paddingHorizontal: 8, paddingTop: 8 }}>
            <Text style={{ textAlign: "left", fontWeight: "600", paddingBottom: 8, fontSize: 18 }}>
              {item.title}
            </Text>
            <Text style={{ textAlign: "left" }}>
              {item.description}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  )
}
