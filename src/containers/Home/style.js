import { colors } from 'config/colors'
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgBody
  },
  containerFlatList: {
    justifyContent: "space-between"
  },
  floatingButton: {
    flex: 1,
    resizeMode: 'contain'
  },
  buttonFLoatingContainer: {
    position: 'absolute',
    flex: 1,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: 'green',
    elevation: 10,
    borderRadius: 30
  }
})
