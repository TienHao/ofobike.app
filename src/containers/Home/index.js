
// libary
import { HeaderComp } from 'components/Header';
import React from 'react';
import { StatusBar, View, TouchableOpacity, Clipboard } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from './style';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import firebase from 'react-native-firebase';

// components
import OrderInProgress from '../OrderInProgress';
import OrderComplete from '../OrderComplete';
import CancelOrder from '../CancelOrder';
import LocalPushNotification from '../OfoNotification/LocalNotification';
import OfoNotification from '../OfoNotification';

// constanst
import {
  SHIPPER,
  RENDER_PAGE_CANCEL_ORDER,
  RENDER_PAGE_IN_PROGRESS_ORDER,
  RENDER_PAGE_COMPLETE_ORDER,
  SHIPPER_OUT_OF_MONEY_PUSH_NOTI,
  RESET_PUSH_NOTI
} from '../../constanst';

// actions
import { changeStatusPushNotification } from '../../actions';


class HomeScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = this._getInitialState();
  }

  _getInitialState = () => {
    return {
      valueCheckPageRender: RENDER_PAGE_COMPLETE_ORDER,
      valueCheckPushNoti: 0
    }
  }

  componentDidMount() {
    const { navigation, userInfo } = this.props;

    const moneyShipper = parseInt(userInfo.deposit);

    if (moneyShipper < 0) {
      this.props.changeStatusPushNotification({ valueCheckPushNoti: 4 }, SHIPPER_OUT_OF_MONEY_PUSH_NOTI);
    }

    //this.props.changeStatusPushNotification({ valueCheckPushNoti: 0 }, SHIPPER_OUT_OF_MONEY_PUSH_NOTI);

    this.focusListener = navigation.addListener('willFocus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBarStyle('light-content');

      this.forceUpdate();
    });

  }

  handleNotification = () => {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          firebase
            .messaging()
            .subscribeToTopic(`push-notification`);
          firebase
            .messaging()
            .getToken()
            .then(function (token) {
              Clipboard.setString(token);
            })
        } else {
          firebase
            .messaging()
            .requestPermission()
            .then(() => {
              firebase
                .messaging()
                .subscribeToTopic(`push-notification`);

              firebase
                .messaging()
                .getToken()
                .then(function (token) {
                  Clipboard.setString(token);
                })
              firebase.messaging().onMessage(function (listener) {
                console.log('Message listener', listener);
              })
            })
            .catch(error => {
              console.log('error request permision', error);
            })
        }
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.valueCheckPushNoti !== prevProps.valueCheckPushNoti) {
      this.forceUpdate();
    }
  }

  messageListener() {
    firebase.messaging().onMessage(function (payload) {
      console.log('paload ', payload);
    });
  }

  componentWillUnmount() {
    this.messageListener();
    this.focusListener.remove();
  }

  checkPageRender = (valueCheck) => {
    this.setState({
      valueCheckPageRender: valueCheck
    })
  }

  onpressCreateOrder = () => {
    this.props.navigation.navigate("Order");
  }

  render() {
    const { userInfo } = this.props;
    const { valueCheckPushNoti } = this.props;

    console.log('this.props.addressDeliver', this.props.addressDeliver);
    console.log('this.props.addressReceiver', this.props.addressReceiver);

    let message = valueCheckPushNoti === 1 ? 'Bạn chọn đơn hàng thành công'
      : valueCheckPushNoti === 2 ? 'Bạn hủy đơn hàng thành công'
        : valueCheckPushNoti === 3 ? 'Bạn Giao đơn hàng thành công'
          : valueCheckPushNoti === 4 ? 'Tiền thưởng và đặt cọc của bạn hết tiền xin  vui lòng đến nhà cung cấp để nạp thêm tiền'
            : '';

    return (
      <View style={styles.container}>
        <HeaderComp
          callbackCheckPageRender={this.checkPageRender}
          {...this.props} />
        {
          this.state.valueCheckPageRender === RENDER_PAGE_IN_PROGRESS_ORDER ?
            (
              <OrderInProgress
                accessToken={this.props.accessToken}
                userInfo={userInfo}
                {...this.props} />
            ) : this.state.valueCheckPageRender === RENDER_PAGE_COMPLETE_ORDER ? (
              <OrderComplete {...this.props} />
            ) : this.state.valueCheckPageRender === RENDER_PAGE_CANCEL_ORDER ? (
              <CancelOrder {...this.props} />
            ) : null
        }
        {userInfo.type_id == SHIPPER ? (
          null
        ) : (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={this.onpressCreateOrder}
              style={styles.buttonFLoatingContainer}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <IconFontAwesome name='cart-plus' style={[{ fontSize: 25, color: 'white' }]} />
              </View>
            </TouchableOpacity>
          )}
        {
          valueCheckPushNoti > 0 && valueCheckPushNoti < 5 ? (
            <LocalPushNotification checkPusNotification={valueCheckPushNoti} message={message} title="Thông báo" />
          ) : null
        }
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { accessToken, userInfo } = state.authReducer;
  return {
    accessToken,
    userInfo,
    valueCheckPushNoti: state.notiReducer.valueCheckPushNoti,
    addressDeliver: state.mapReducer.addressDeliver,
    addressReceiver: state.mapReducer.addressReceiver,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeStatusPushNotification }, dispatch);
}

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
export { HomeContainer };

