import { callApi, getApiHistories } from 'api';
import { Loading } from 'components/Loading';
import React, { Fragment, PureComponent } from 'react';
import {
  FlatList,
  StatusBar,
  Text,
  TouchableWithoutFeedback,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from './style';
import { getStatusBarHeight } from 'react-native-status-bar-height';

// constants
import { SHIPPER, CUSTOMMER } from '../../constanst';

// utils
import { currencyFormat } from 'utils';

class HistoryScreen extends PureComponent {
  constructor(props) {
    super(props);

    this.state = this._getInitialState();
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      this._configStatusBar();

      const isCancelOrder = this.props.navigation.getParam("cancelOrder", false);

      this.forceUpdate();

      if (isCancelOrder) {
        this.setState({ ...this._getInitialState() }, () => this._getHistories());
      }
    });

    this._getHistories();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userInfo !== this.props.userInfo || prevState.histories.length !== this.state.histories.length) {
      this._getHistories();
    }
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _configStatusBar = () => {
    StatusBar.setTranslucent(true);
    StatusBar.setBarStyle('dark-content');
  }

  _getInitialState = () => ({
    histories: [],
    userInfo: "",
    isLoading: true,
    errorMessage: ""
  })

  _getHistories = async () => {
    const { userInfo } = this.props;

    if (userInfo) {
      const authorization = this.props.accessToken;

      let res = null;

      if (userInfo.type_id == SHIPPER) {
        res = await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 1 }));
      } else {
        res = await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 0 }));
      }

      if (res.success) {
        const histories = res.data;

        let historyShippers = [];

        histories.forEach(function (history) {
          if (history.status_id === '3' && userInfo.type_id == SHIPPER) {
            historyShippers.push(history);
          } else if (userInfo.type_id == CUSTOMMER || userInfo.type_id == undefined) {
            historyShippers.push(history);
          }
        });

        this.setState({ userInfo, histories: historyShippers, isLoading: false });
      } else {
        this.setState({ errorMessage: "Xảy ra lỗi", isLoading: true });
      }

    } else {
      this.setState({ errorMessage: "Bạn chưa đăng nhập", isLoading: true });
    }
  }

  _onPressItem = (item) => {
    this.props.navigation.navigate('HistoryDetail', { history: { ...item } });
  }

  _cancelOrder = (item) => {
    this.props.navigation.navigate("CancelOrder", { history: item });
  }

  shipperChooseOrder = (item) => {
    this.props.navigation.navigate("ShipperChooserOrder", { history: item });
  }

  _renderHistoryItem = ({ item, index }) => {
    const { userInfo } = this.props;

    return (
      <View style={{ backgroundColor: 'white', marginBottom: 15 }}>
        <TouchableWithoutFeedback onPress={() => this._onPressItem(item)}>
          <View style={{ padding: 15 }}>
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Text>{item.code}</Text>
              </View>
              <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{currencyFormat(item.fee)} VNĐ</Text>
              </View>
            </View>

            <View style={{ marginTop: 10, marginBottom: 10 }}>
              <View>
                <Text>{item.address_from}</Text>
              </View>
              <View>
                <Text>{item.address_to}</Text>
              </View>
            </View>

            <View style={{ marginBottom: 5 }}>
              <Image
                resizeMode='stretch'
                source={{ uri: item.image }}
                style={{ height: 100, width: 120 }}
              />
            </View>

            <View style={{ flexDirection: 'row' }}>
              {userInfo.type_id == CUSTOMMER ?
                (item.status_id <= 1 &&
                  <View style={{ backgroundColor: '#FCB300', width: 130, borderWidtt: 0, padding: 7, elevation: 3, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      onPress={() => {
                        let result = this._cancelOrder(item);

                        return result;
                      }}
                    >
                      <Text style={{ color: 'white' }}>
                        {"Hủy đơn hàng"}
                      </Text>
                    </TouchableOpacity>
                  </View>)
                : null
              }
              <View style={{ flex: 7, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{item.status_name}</Text>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

  renderHeader() {
    const { userInfo } = this.props;

    return (
      <View style={[{ flexDirection: 'row', marginTop: getStatusBarHeight(), height: 45, borderWidth: 0, elevation: 5, backgroundColor: 'white', shadowOpacity: 5, shadowColor: 'grey' }]}>
        <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
          <Text style={{ fontSize: 20 }}>{userInfo.type_id === CUSTOMMER ? "Lịch sử Đơn hàng" : "Đơn hàng"}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { isLoading, errorMessage } = this.state;
    const { userInfo } = this.props;

    return (
      <Fragment>
        {
          isLoading
            ?
            <View style={{ flex: 1 }}>
              {this.renderHeader()}
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                {!errorMessage
                  && <Loading />
                  || <Text style={{ textAlign: "center" }}>{errorMessage}</Text>
                }
              </View>
            </View>
            :
            <View style={styles.container}>
              {this.renderHeader()}
              {
                this.state.histories.length ?
                  <FlatList
                    initialNumToRender={10}
                    maxToRenderPerBatch={4}
                    updateCellsBatchingPeriod={5000}
                    data={this.state.histories}
                    extraData={this.state}
                    numColumns={1}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={this._renderHistoryItem}
                  />
                  :
                  userInfo.type_id == SHIPPER ? (
                    <View style={{ flex: 1, justifyContent: "center" }}>
                      <Text style={{ textAlign: "center", color: 'black', fontWeight: 'bold' }}>Hiện tại bạn chưa có đơn hàng hoàn thành!</Text>
                    </View>
                  ) : (
                      <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ textAlign: "center", color: 'black', fontWeight: 'bold' }}>Bạn chưa có đơn hàng!</Text>
                      </View>
                    )
              }
            </View>
        }
      </Fragment>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken, userInfo } = state.authReducer;
  return { accessToken, userInfo };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

const HistoryContainer = connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
export { HistoryContainer };

