// libary
import {
  callApi,
  getApiHistoryTree,
  updateStatusDelivery,
  getInfoShipper,
  postRateComment
} from 'api';
import { Loading } from 'components/Loading';
import React, { PureComponent } from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from '../style';
import getDirections from 'react-native-google-maps-directions';
import Geolocation from 'react-native-geolocation-service';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

// components
import OfoTextInput from '../../../components/OfoTextInput';

const RNFS = require('react-native-fs');

// constanst
import { SHIPPER, CHANGE_STATUS_CONFRIM_ORDER_PUSH_NOTI } from '../../../constanst';

// utils
import { takePermission, permissionType, currencyFormat } from 'utils';

// action
import { changeStatusPushNotification } from '../../../actions';

const options = {
  title: 'Chọn hình',
  takePhotoButtonTitle: "Chụp hình",
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class HistoryDetailScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Chi tiết vận đơn',
    };
  };

  constructor(props) {
    super(props);

    this.state = this._getInitialSate();

    const { navigation } = this.props;
    this._history = navigation.getParam('history', 'some default value');
    this._initializeAsync();
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      this._configStatusBar();
    });

    this.getCurrentLocation();
  }

  chooseImage = async () => {
    const acceptPhoto = await takePermission(permissionType.photo);
    const authorization = this.props.accessToken;
    const { code } = this._history;

    if (!acceptPhoto) {
      this.setState({ errorMessagePhoto: "Chức năng này cần phải được cho phép truy cập ảnh" });
      return
    } else {
      this.setState({ errorMessagePhoto: "" });
    }

    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        ImageResizer.createResizedImage(response.uri, 400, 400, 'JPEG', 72)
          .then(async (imgResize) => {
            const base64Data = await RNFS.readFile(
              imgResize.path,
              "base64"
            );
            const res = await callApi(updateStatusDelivery({ authorization, data: { code: code, status_id: 3, image_after: `data:image/jpeg;base64,${base64Data}` } }));
            if (res.success) {
              await this.props.changeStatusPushNotification({ valueCheckPushNoti: 3 }, CHANGE_STATUS_CONFRIM_ORDER_PUSH_NOTI);
            }
            await this.props.navigation.navigate("Home", { cancelOrder: true });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  }

  onEndEditingCheckError = () => {

  }

  _initializeAsync = async () => {
    const authorization = this.props.accessToken;
    const { userInfo } = this.props;
    const { code } = this._history;

    const res = await callApi(getApiHistoryTree({ authorization, code: code }));

    let resInforShipper = null;

    try {
      if (code !== undefined) {
        let info = await AsyncStorage.getItem(code);

        if (userInfo.type_id !== SHIPPER && (res.data.status_id > 1 && res.data.status_id <= 4)) {
          let resData = await callApi(getInfoShipper({ authorization, data: { "id": JSON.parse(info).member_id } }));

          resInforShipper = resData.data;
        }
      }
    } catch (error) {
      console.log('error', error);
    }

    if (res.success) {
      await this.setState({
        destinationLocation: {
          latitude: res.data.location.destination.lat,
          longitude: res.data.location.destination.lng
        },
        originLocation: {
          latitude: res.data.location.origin.lat,
          longitude: res.data.location.origin.lng
        },
        infoDetailOrder: {
          ...this.state.infoDetailOrder,
          mass: res.data.mass,
          note: res.data.note,
          product_infor: res.data.product_infor,
          receiver_fullname: res.data.receiver_fullname,
          receiver_phone: res.data.receiver_phone,
          receiver_time: res.data.receiver_time,
          receiver_date: res.data.receiver_date,
          sender_fullname: res.data.sender_fullname,
          sender_phone: res.data.sender_phone,
          sender_time: res.data.sender_time,
          sender_date: res.data.sender_date,
          status_id: res.data.status_id,
          infoShipper: resInforShipper,
          status_name: res.data.status_name,
          image: res.data.image
        }
      })
    }
  }

  getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { coords } = position;
        this.setState({
          currentLocation: {
            ...this.state.currentLocation,
            latitude: coords.latitude,
            longitude: coords.longitude
          },
          isGetCurrentLocation: true
        })
      },
      (error) => {
        this.setState({ permissionLocation: false });
        this.setState({ permissionMessage: "Request time out!" });
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _getInitialSate = () => ({
    isLoading: false,
    currentLocation: {
      latitude: 0,
      longitude: 0
    },
    destinationLocation: {
      latitude: 0,
      longitude: 0
    },
    originLocation: {
      latitude: 0,
      longitude: 0
    },
    infoDetailOrder: {
      mass: 0,
      note: '',
      product_infor: '',
      receiver_fullname: '',
      receiver_phone: '',
      receiver_time: '',
      receiver_date: '',
      sender_fullname: '',
      sender_phone: '',
      sender_time: '',
      sender_date: '',
      infoShipper: null,
      status_name: ''
    },
    reviewRating: 0,
    reviewContent: '',
    isOpenCamera: false
  });

  _configStatusBar = () => {
    StatusBar.setTranslucent(true);
    StatusBar.setBarStyle('dark-content');
  }

  _viewHistoryTree = () => {
    this.props.navigation.navigate('HistoryTree', { history: this._history.code });
  }

  viewGoogleMapTrackingStreesGoReceiver = () => {
    const { currentLocation, destinationLocation } = this.state;

    const data = {
      source: currentLocation,
      destination: destinationLocation,
      params: [
        {
          key: "travelmode",
          value: "driving"
        },
        {
          key: "dir_action",
          value: "navigate"
        }
      ]
    }

    getDirections(data);
  }

  viewGoogleMapTrackingStreesGoSender = () => {
    const { currentLocation, originLocation } = this.state;

    const data = {
      source: currentLocation,
      destination: originLocation,
      params: [
        {
          key: "travelmode",
          value: "driving"
        },
        {
          key: "dir_action",
          value: "navigate"
        }
      ]
    }

    getDirections(data);
  }

  rateStart(review, index) {
    if (review > index - 1 && review < index) {
      return "ios-star-half";
    }
    if (review >= index) {
      return "ios-star";
    }
    if (review < index) {
      return "ios-star-outline";
    }
    return "ios-star";
  }

  onpressRating(rate) {
    this.setState({ reviewRating: rate });
  }

  onPressSendRating = async () => {
    const authorization = this.props.accessToken;
    if (this.state.reviewContent !== "" && this.state.reviewRating !== 0) {

      let data = {
        code: this._history.code,
        star: this.state.reviewRating,
        comment: this.state.reviewContent
      };

      const res = await callApi(postRateComment({ authorization, data }));

      if (res.success) {
        ToastAndroid.show('Gửi thành công', ToastAndroid.LONG);
        this.setState({
          reviewContent: '',
          reviewRating: 0
        })
      }
    } else {
      ToastAndroid.show('Bạn chưa nhập text hoặc chưa đánh giá cấp độ!', ToastAndroid.LONG);
    }
  }

  onChangeText = (name, value) => {
    this.setState({
      reviewContent: value
    })
  }

  openCaptureImage = () => {
    this.setState({
      isOpenCamera: true
    })
  }

  render() {
    const { isLoading, infoDetailOrder } = this.state;
    const {
      code,
      address_from,
      address_to,
      status_name
    } = this._history;

    const { userInfo } = this.props;

    return (
      isLoading
      &&
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Loading />
      </View>
      ||
      <React.Fragment>
        <ScrollView>
          <View style={{ backgroundColor: 'gray' }}>
            <Text style={{ color: 'white', padding: 5, fontWeight: 'bold', fontSize: 16, marginLeft: 10 }}>{`${status_name}`}</Text>
          </View>
          <View style={styles.containerForm}>
            <View style={{ flex: 1 }}>
              <Text style={{ marginTop: 32 }}>Mã vận đơn: {code}</Text>
            </View>
            <View>
              <Text style={styles.titleBlock}>
                Người giao
          </Text>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Họ tên</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.sender_fullname}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Ngày giao</Text>
                </View>
                <View style={styles.col}>
                  <Text>{moment(infoDetailOrder.sender_date).format('DD/MM/YYYY')}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Thời gian giao</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.sender_time}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Số điện thoại</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.sender_phone}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Địa chỉ</Text>
                </View>
                <View style={styles.col}>
                  <Text>
                    {address_from}
                  </Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Hình ảnh</Text>
                </View>
                <View style={styles.col}>
                  <View style={{ marginBottom: 5 }}>
                    <Image
                      resizeMode='stretch'
                      source={{ uri: infoDetailOrder.image }}
                      style={{ height: 100, width: 120 }}
                    />
                  </View>
                </View>
              </View>

              {userInfo.type_id === SHIPPER && infoDetailOrder.status_id == '2' ? (
                <View>
                  <View>
                    <TouchableOpacity
                      onPress={() => this.viewGoogleMapTrackingStreesGoSender()}
                      style={{ backgroundColor: '#FCB300', padding: 15, marginTop: 8 }}
                    >
                      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Text style={{ color: 'white' }}>Mở map chỉ dẫn đường đến điểm nhận hàng</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>

            <View>
              <Text style={styles.titleBlock}>
                Người Nhận
          </Text>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Họ tên</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.receiver_fullname}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Số điện thoại</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.receiver_phone}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Ngày nhận</Text>
                </View>
                <View style={styles.col}>
                  <Text>{moment(infoDetailOrder.receiver_date).format('DD/MM/YYYY')}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Thời gian giao</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.receiver_time}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Địa chỉ</Text>
                </View>
                <View style={styles.col}>
                  <Text>{address_to}</Text>
                </View>
              </View>

              {(userInfo.type_id !== SHIPPER && infoDetailOrder.status_id > 1 && infoDetailOrder.infoShipper) ? (
                <View>
                  <Text style={styles.titleBlock}>
                    Thông tin shipper
                </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={styles.col}>
                      <Image
                        resizeMode="stretch"
                        style={{
                          borderWidth: 1,
                          width: 50,
                          height: 50
                        }}
                        source={infoDetailOrder.infoShipper.avatar !== null ? { uri: infoDetailOrder.infoShipper.avatar } : require('../../../images/avatar_default.png')} />
                    </View>
                    <View style={[styles.col, { justifyContent: 'center', flex: 5 }]}>
                      <Text>{infoDetailOrder.infoShipper.fullname}</Text>
                    </View>
                  </View>
                </View>
              ) : null}

              {userInfo.type_id === SHIPPER && infoDetailOrder.status_id == '2' ? (
                <View>
                  <View>
                    <TouchableOpacity
                      onPress={() => this.viewGoogleMapTrackingStreesGoReceiver()}
                      style={{ backgroundColor: '#FCB300', padding: 15, marginTop: 8 }}
                    >
                      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Text style={{ color: 'white' }}>Mở map chỉ dẫn đường đến điểm giao hàng</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>

            <View>
              <Text style={styles.titleBlock}>
                Thông tin hàng
          </Text>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Tên hàng</Text>
                </View>
                <View style={styles.col}>
                  <Text>{infoDetailOrder.product_infor}</Text>
                </View>
              </View>

              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Khối lượng</Text>
                </View>
                <View style={[styles.col]}>
                  <Text>{infoDetailOrder.mass} kg</Text>
                </View>
              </View>
            </View>

            <View>
              <Text style={styles.titleBlock}>
                Thông tin dịch vụ và cước phí
          </Text>
              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Phí ship do</Text>
                </View>
                <View style={styles.col}>
                  <Text>{this._history.ship_money_type === 0 ? "Người giao trả" : "Người nhận trả"}</Text>
                </View>
              </View>
            </View>

            <View>
              <Text style={styles.titleBlock}>
                Cước phí
          </Text>
              <View style={[styles.row, styles.line]}>
                <View style={styles.col}>
                  <Text>Tổng tiền</Text>
                </View>
                <View style={styles.col}>
                  <Text>{currencyFormat(this._history.fee)} VNĐ</Text>
                </View>
              </View>
            </View>
            <View style={{ paddingVertical: 8 }}></View>
          </View>
          {userInfo.type_id !== SHIPPER && infoDetailOrder.status_id == '3' ? (
            <View style={{ flex: 1, paddingLeft: 16, paddingRight: 26, paddingBottom: 16 }}>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }} >{"Đánh Giá"}</Text>
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <View style={{ justifyContent: 'center' }}>
                  <Text>{"Chất lượng: "} </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity onPress={() => this.onpressRating(1)}>
                    <Ionicons
                      name={this.rateStart(this.state.reviewRating, 1)}
                      size={30}
                      style={{ color: '#ff9900', marginRight: 3 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onpressRating(2)}>
                    <Ionicons
                      name={this.rateStart(this.state.reviewRating, 2)}
                      size={30}
                      style={{ color: '#ff9900', marginRight: 3 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onpressRating(3)}>
                    <Ionicons
                      name={this.rateStart(this.state.reviewRating, 3)}
                      size={30}
                      style={{ color: '#ff9900', marginRight: 3 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onpressRating(4)}>
                    <Ionicons
                      name={this.rateStart(this.state.reviewRating, 4)}
                      size={30}
                      style={{ color: '#ff9900', marginRight: 3 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onpressRating(5)}>
                    <Ionicons
                      name={this.rateStart(this.state.reviewRating, 5)}
                      size={30}
                      style={{ color: '#ff9900', marginRight: 3 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }} >
                <View style={{ justifyContent: 'center', flex: 3 }}>
                  <Text>{"Bình Luận"}: </Text>
                </View>
                <OfoTextInput
                  placeholder={"Nhập bình luận"}
                  styles={{ flex: 7 }}
                  multiline={true}
                  name="comment"
                  name={'rating'}
                  value={this.state.reviewContent}
                  onChangeText={this.onChangeText}
                  onEndEditingCheckError={this.onEndEditingCheckError}
                />
                <TouchableOpacity style={{
                  borderWidth: 1,
                  padding: 5,
                  height: 33,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#FCB300',
                  borderColor: '#FCB300',
                }} onPress={this.onPressSendRating}>
                  <Text style={{ color: "white", textAlign: "center" }}>{"Gửi"}</Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          {
            userInfo.type_id == SHIPPER && infoDetailOrder.status_id == '2' ? (
              null
            ) : (
                <TouchableOpacity
                  style={[styles.btn, { marginLeft: 8, marginRight: 8 }]}
                  onPress={() => {
                    let result = this._viewHistoryTree();

                    return result;
                  }}
                >
                  <Text style={styles.btnText}>{"Xem thông tin vận chuyển"}</Text>
                </TouchableOpacity>
              )
          }
          <View style={{ paddingVertical: 8 }}></View>
        </ScrollView>
        {
          userInfo.type_id == SHIPPER && infoDetailOrder.status_id == '2' ? (
            <TouchableOpacity
              onPress={() => this.chooseImage()}
              activeOpacity={0.7}
              style={styles.buttonFLoatingContainer}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <IconAntDesign name='camera' style={[{ fontSize: 25, color: 'white' }]} />
              </View>
            </TouchableOpacity>
          ) : null
        }
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken, userInfo } = state.authReducer;
  return { accessToken, userInfo };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeStatusPushNotification }, dispatch);
}

const HistoryDetailContainer = connect(mapStateToProps, mapDispatchToProps)(HistoryDetailScreen);
export { HistoryDetailContainer };

