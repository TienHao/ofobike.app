// libary
import { callApi, getApiCancelOrder } from 'api';
import { colors } from 'config/colors';
import { MyInputText } from 'controls';
import React, { PureComponent } from 'react';
import { findNodeHandle, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myRule } from 'utils';

// constanst
import { CHANGE_STATUS_CANCEL_ORDER_PUSH_NOTI } from '../../../constanst';

// actions
import { changeStatusPushNotification } from '../../../actions';

class CancelOrderScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Yêu cầu hủy vận đơn'
    };
  };

  constructor(props) {
    super(props)

    this._validForm = []
    this._positionControls = []

    this._history = this.props.navigation.getParam('history', 'some default value');
    this.state = this._getInitialState()
  }

  _getInitialState = () => ({
    ...this._history,
    reasonCancel: "",
    isCheckValid: false,
    isActiveToast: false,
    messageToast: ""
  })

  _handleChange = ({ name, value }) => {
    this.setState({ [name]: value });
  }

  _scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true })
  }

  _calculatePosition = (name) => {
    this._positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this._scrollTo({ x, y });
      }
    )
  }

  _checkValidate = async () => {
    try {
      this._validForm = []
      await this._setStateAsync({ isCheckValid: true })

      return {
        valid: this._validForm.every(control => {
          if (!control.valid) {
            this._calculatePosition(control.name)
            return false
          }
          return true
        }),
        data: {
          code: this.state.code,
          note: this.state.reasonCancel
        }
      }

    } catch (error) {
      return false
    }
  }

  _cancelOrder = async () => {
    const authorization = this.props.accessToken;
    const { valid, data } = await this._checkValidate();

    if (valid) {
      const res = await callApi(getApiCancelOrder({ authorization, data }));
      if (res && res.success) {
        this.props.navigation.navigate("Home", { cancelOrder: true });
        this.props.changeStatusPushNotification({ valueCheckPushNoti: 2 }, CHANGE_STATUS_CANCEL_ORDER_PUSH_NOTI);        
      } else {
        this.setState({ messageToast: "Có lỗi xảy ra", isActiveToast: true, toastType: "error" })
      }
    }
  }

  _timeEnd = () => {
    this.setState({ isActiveToast: false, messageToast: "" });
  }

  render() {
    const { isCheckValid } = this.state;

    return (
      <>
        <ScrollView
          ref={el => this._scrollView = el}
          style={{ flex: 1, paddingHorizontal: 16, backgroundColor: colors.bgBody, paddingTop: 16 }}
        >

          <View>
            <MyInputText
              name={"code"}
              label={"Mã vận đơn"}
              value={this.state.code}
              editable={false}
            />
          </View>
          <View
            ref={el => this._positionControls["reasonCancel"] = el}
          >
            <MyInputText
              name={"reasonCancel"}
              label={"Lý do"}
              placeholder={"Nhập lý do hủy vận đơn"}
              value={this.state.reasonCancel}

              rules={[myRule.required]}
              onChangeText={this._handleChange}
              checkValidForm={isCheckValid}
              onValidate={(control) => this._validForm.push(control)}
            />
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this._cancelOrder()}
            >
              <View
                style={{
                  backgroundColor: colors.primary.default,
                  padding: 16
                }}
              >
                <Text style={{ textAlign: "center", color: "#fff" }}>Hủy vận đơn</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken } = state.authReducer;
  return { accessToken };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeStatusPushNotification }, dispatch);
}

const CancelOrderContainer = connect(mapStateToProps, mapDispatchToProps)(CancelOrderScreen);
export { CancelOrderContainer };

