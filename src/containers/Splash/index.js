import NetInfo from "@react-native-community/netinfo";
import { getAccessTokenAction } from 'actions';
import React, { PureComponent } from 'react';
import { ActivityIndicator, Image, StatusBar, Text, View } from 'react-native';
import { SwitchActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getWidthPercentage, permissionType, takePermission } from 'utils';

class SplashScreen extends PureComponent {
  constructor(props) {
    super(props);

    this.state = this._getInitialState();
  }

  componentDidMount() {
    try {
      this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
        StatusBar.setHidden(true);
        StatusBar.setTranslucent(true);
        StatusBar.setBackgroundColor("transparent");

        this._unsubscribe = NetInfo.addEventListener(state => {
          const { isConnected } = state
          if (!isConnected) {
            this.setState({ netMessage: "Không có kết nối mạng" });

          } else {
            this.setState({ netMessage: "" })
            this.props.getAccessTokenAction()
          }
        });
      });

      this._navListenerBlur = this.props.navigation.addListener('willBlur', () => {
        StatusBar.setHidden(false)
      });
    } catch (error) {
      // error
    }
  }

  componentDidUpdate() {
    if (this.props.auth.error && process.env.NODE_ENV === "development") {
      this._appError(this.props.auth.error)
    }

    if (!this.props.auth.isLoading) {
      setTimeout(() => {
        this._appReady();
      }, 2000);
    }
  }

  _appError = (error) => {
    const { navigation } = this.props
    navigation.navigate('Error', { error });
  }

  _appReady = async () => {
    await takePermission(permissionType.photo)
    await takePermission(permissionType.camera)
    await takePermission(permissionType.location)

    const routeName = "Main"
    this.props.navigation.dispatch(SwitchActions.jumpTo({ routeName }));
    StatusBar.setHidden(false)
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
    this._navListenerBlur.remove();
  }

  _getInitialState = () => {
    return {
      netMessage: ""
    }
  }

  render() {
    const percentLogo = getWidthPercentage(70);
    const { netMessage } = this.state;

    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#fed9a2' }}>
        <Image
          style={{ flex: 1, resizeMode: "contain" }}
          source={require('../../images/loading_app.png')}
        />
        {
          !netMessage
          && <ActivityIndicator size="small" color={"#fff"} />
          ||
          <View>
            <Text style={{ textAlign: "center", color: "#fff", padding: 16 }}>{netMessage}</Text>
          </View>
        }
      </View>
    );
  }
}

function mapStateToProps(state) {
  const auth = state.authReducer
  return {
    auth
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getAccessTokenAction }, dispatch)
}

const SplashContainer = connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
export { SplashContainer };

