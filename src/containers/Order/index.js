// libary
import React, { Fragment } from 'react';
import {
  BackHandler,
  Keyboard,
  KeyboardAvoidingView,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { customAlert, permissionType, takePermission } from 'utils';
import {
  FinishStep,
  OrderStep1,
  OrderStep2,
  OrderStep3,
  OrderStep4
} from './components/index';
import { styles } from "./style";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { compareTwoString } from '../../utilities';

// services
import { callApi, submitStep } from 'api';

// components
import { Loading } from 'components/Loading';

// actions
import { getAddressDelivery } from '../../actions';
import { getFee } from '../../actions';

class OrderScreen extends React.Component {
  constructor(props) {
    super(props);

    takePermission(permissionType.photo);
    this.state = this.getInitialState();
    this.initializeLocal();
    this.configStatusBar();

    this.buttonHandle = false;
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  componentDidUpdate(prevProps) {
    let checkAddressDeliver = compareTwoString(prevProps.addressDeliver, this.props.addressDeliver);
    let checkAddressReceiver = compareTwoString(prevProps.addressReceiver, this.props.addressReceiver);

    if (checkAddressDeliver !== 0) {
      this.setState({
        senderAddress: this.props.addressDeliver
      })
    }

    if (checkAddressReceiver !== 0) {
      this.setState({
        recipientAddress: this.props.addressReceiver
      })
    }

    this.initializeLocal();
  }

  setCurrentStep = () => {
    this.setState({
      currentStep: 1
    });
  }

  initializeLocal = () => {
    this.mapStep = {
      1: {
        title: "Thông tin giao - nhận",
        component: <OrderStep1 {...this.props} getDataByStep={this.getDataByStep} ref={el => this._OrderStep1 = el} />
      },
      2: {
        title: "Hàng hóa và vận chuyển",
        component: <OrderStep2 {...this.props} getDataByStep={this.getDataByStep} ref={el => this._OrderStep2 = el} />
      },
      3: {
        title: "Cước phí",
        component: <OrderStep3 {...this.props} getDataByStep={this.getDataByStep} ref={el => this._OrderStep3 = el} />
      },
      4: {
        title: "Hoàn tất",
        component: <OrderStep4 {...this.props} getDataByStep={this.getDataByStep} ref={el => this._OrderStep4 = el} />
      },
      5: {
        title: "Thành công",
        component: <FinishStep callbackSetCurrentStep={this.setCurrentStep} {...this.props} getDataByStep={this.getDataByStep} ref={el => this._FinishStep = el} />
      }
    }

  }

  configStatusBar = () => {
    StatusBar.setTranslucent(true);
    StatusBar.setBarStyle('dark-content');
  }

  getDataByStep = (step) => {
    return this.state[`step${step}`];
  }

  getInitialState = () => {
    const { userInfo } = this.props;

    return {
      isLoading: false,
      isVisibleKeyBoard: true,
      currentStep: 1,
      distance: 0,
      step1: {
        memberId: userInfo.id || undefined,
        senderName: userInfo.fullname || "",
        senderPhone: userInfo.phone || "",
        senderEmail: userInfo.email || "",
        senderAddress: "",
        senderDate: '',
        senderTime: '',
        recipientName: "",
        recipientPhone: "",
        recipientEmail: "",
        recipientAddress: "",
        receiverDate: "",
        receiverTime: ""
      },
      step2: {
        packageName: "",
        weights: "",
        COD: "",
        descriptions: "",
        note: "",
        service: "",
        responseData: "",
        packageImage: ""
      },
      step3: {
        responseData: ""
      },
      step4: {
        responseData: ""
      },
      finishStep: {
        responseData: ""
      }
    }
  }

  keyboardWillShow = event => {
    this.setState({
      isVisibleKeyBoard: false
    })
  }

  keyboardWillHide = event => {
    this.setState({
      isVisibleKeyBoard: true
    })
  }

  handleBackPress = () => {
    const { currentStep } = this.state;
    if (currentStep === 5 || !this.props.userInfo) return;

    customAlert({
      title: 'Hủy vận đơn',
      message: 'Bạn chắc chắn muốn hủy vận đơn',
      handleCancel: () => null,
      handleOK: () => {
        this.props.navigation.navigate("Home")
        this.props.getAddressDelivery({ addressDeliver: '' });
        this.props.getAddressDelivery({ addressReceiver: '' });
        this.props.getFee({
          currentFee: undefined,
          distance: undefined
        });
      }
    })
    return true;
  }

  renderStep = () => {
    const { currentStep } = this.state;
    return this.mapStep[currentStep].component;
  }

  nextStep = async () => {
    const { currentStep } = this.state;

    const nextStep = currentStep + 1;

    let { valid, data } = await this[`_OrderStep${currentStep}`].checkValidate();


    if (valid && this.mapStep[nextStep] && !this.buttonHandle) {
      this.buttonHandle = true;
      this.setState({ isLoading: true });

      const res = await this.handleSubmitStep(currentStep, data);

      if (res.success) {
        this.setState({
          [`step${currentStep}`]: data,
          [`step${nextStep}`]: { ...this.state[`step${nextStep}`], responseData: res.data, isLoading: false },
          currentStep: nextStep
        });
      }

      this.buttonHandle = false;
      this.setState({ isLoading: false });
    }
  }

  handleSubmitStep = async (step, data) => {
    const authorization = this.props.accessToken;

    const apiSubmitStep = submitStep({ step, data, authorization });

    return await callApi(apiSubmitStep, true);
  }

  backStep = () => {
    const { currentStep } = this.state;
    const prevStep = currentStep - 1;

    if (this.mapStep[prevStep]) {
      this.setState({ currentStep: prevStep });
    }
  }

  renderHeaderStep = () => {
    const { currentStep } = this.state;

    if (currentStep === 5) return (
      <Fragment>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={[styles.stepText, { fontWeight: 'bold', fontSize: 16 }]}>
            {"Bạn đã tạo đơn hàng thành công"}
          </Text>
        </View>
      </Fragment>
    )

    return (
      <Fragment>
        <View style={{ marginRight: 16, marginLeft: 5 }}>
          <TouchableWithoutFeedback
            onPress={() => this.handleBackPress()}
          >
            <MaterialCommunityIcons name='arrow-left' style={{ fontSize: 24 }} />
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.stepCircle}>
          <Text style={styles.stepCircleText}>
            {currentStep}
          </Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.stepText}>
            {this.mapStep[currentStep].title}
          </Text>
        </View>
      </Fragment>
    )
  }

  renderButtonBack = () => {
    const { currentStep } = this.state;
    if (currentStep === 5 || currentStep === 1) return null;

    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => this.backStep()}
        >
          <Text style={[styles.button, { elevation: 5, borderWidth: 0, backgroundColor: '#1F2D6A' }]}>Quay lại</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderButtonNext = () => {
    const { currentStep } = this.state;

    if (currentStep === 5) return null;

    return (
      <View style={{ flex: 1 }}>
        <View>
          <TouchableOpacity
            onPress={() => this.nextStep()}
          >
            <Text style={[styles.button, { elevation: 5, borderWidth: 0 }]}>
              {
                currentStep !== 4 &&
                "Tiếp theo" ||
                "Hoàn tất"
              }
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const { isVisibleKeyBoard, isLoading } = this.state;
    const { userInfo } = this.props;

    const price = this.state.distance > 3 ? (3 * 4000) + (this.state.distance) * 17000 : 17000;
    const km = this.state.km > 3 ? this.state.km : 3;

    return (
      userInfo &&
      <SafeAreaView style={{ flex: 1 }}>
        {
          isLoading &&
          <View style={{ flex: 1, justifyContent: "center" }}>
            <Loading />
          </View>
          ||
          <View style={styles.container}>
            <View style={styles.containerStep}>
              {this.renderHeaderStep()}
            </View>

            <View style={[styles.container]}>
              <KeyboardAvoidingView
                style={{ flex: 1 }}
                keyboardVerticalOffset={64}
                behavior="height"
              >
                {this.renderStep()}
              </KeyboardAvoidingView>
            </View>
            <View style={{ height: 95, backgroundColor: 'white', elevation: 5, borderWidth: 0 }}>
              {
                isVisibleKeyBoard
                &&
                <Fragment>
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: 'red' }}>{`${this.props.currentFee == undefined ? "17.000" : this.props.currentFee}VNĐ`}</Text>
                    <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 14 }}>{` (${this.props.distance == undefined ? 3 : this.props.distance}km)`}</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    {

                      <Fragment>
                        {this.renderButtonBack()}
                        {this.renderButtonNext()}
                      </Fragment>
                    }
                  </View>
                </Fragment>
              }

            </View>
          </View>
        }
      </SafeAreaView>
      ||
      <View style={{ flex: 1 }}>
        <View style={styles.containerStep}>
          <TouchableWithoutFeedback
            onPress={() => this.props.navigation.navigate("Account")}
          >
            <MaterialCommunityIcons name='arrow-left' style={{ fontSize: 24, marginLeft: 10 }} />
          </TouchableWithoutFeedback>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>

            <Text style={[styles.stepText, { fontWeight: 'bold', fontSize: 16 }]}>
              {"Bạn chưa đăng nhập"}
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, justifyContent: "center", marginLeft: 16, marginRight: 16, }}>
          <View style={{ justifyContent: "center", marginTop: 16 }}>
            <TouchableOpacity
              style={[styles.button]}
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Text style={styles.btnText}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { accessToken, userInfo } = state.authReducer;
  return {
    userInfo,
    accessToken,
    addressDeliver: state.mapReducer.addressDeliver,
    addressReceiver: state.mapReducer.addressReceiver,
    dateTime: state.orderReducer.dateTime,
    checkGetDateTime: state.orderReducer.checkGetDateTime,
    valueCheckPeople: state.mapReducer.valueCheckPeople,
    location: state.mapReducer.location,
    currentFee: state.orderReducer.currentFee,
    distance: state.orderReducer.distance
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getAddressDelivery, getFee }, dispatch)
}

const OrderContainer = connect(mapStateToProps, mapDispatchToProps)(OrderScreen);

export { OrderContainer };

