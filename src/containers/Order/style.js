import { colors } from 'config/colors';
import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { getWidthPercentage } from 'utils';

export const styles = StyleSheet.create({
  body: {
    backgroundColor: colors.bgBody
  },

  container: {
    flex: 1,
    backgroundColor: colors.bgBody,
  },

  containerForm: {
    paddingHorizontal: 16,
    marginBottom: 40
  },

  containerStep: {
    paddingTop: getStatusBarHeight(),
    marginBottom: 8,
    backgroundColor: "white",
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    height: 70,
    shadowColor: 'gray',
    elevation: 2,
    shadowRadius: 20,
    shadowOffset: { width: 10, height: 10 },
    borderWidth: 0
  },

  containerImageStep2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 16
  },

  coverImageStep2: {
    overflow: "hidden",
    height: getWidthPercentage(80),
    width: getWidthPercentage(90),
    borderStyle: "dashed",
    borderColor: 'grey',
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1
  },

  stepCircle: {
    height: 25,
    width: 25,
    marginRight: 8,
    borderWidth: 2,
    borderColor: colors.primary.default,
    justifyContent: "center",
    alignItems: "center"
  },

  stepCircleText: {
    color: colors.primary.default,
    textAlign: "center",
    fontSize: 13
  },

  stepText: {
    fontSize: 14
  },

  titleBlock: {
    paddingVertical: 0,
    fontSize: 18,
    fontWeight: "bold"
  },

  input: {
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: 8,
    marginBottom: 8
  },

  picker: {
    backgroundColor: "#fff",
    borderRadius: 8,
    overflow: 'hidden'
  },

  button: {
    padding: 12,
    color: "#fff",
    backgroundColor: '#FFA910',
    textAlign: "center",
    margin: 10,
    fontSize: 16
  },

  row: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: -8,

  },

  col: {
    flex: 1,
    marginHorizontal: 8
  },

  label: {
    padding: 2,
    fontSize: 12,
    color: 'grey'
  },

  containerInput: {
    marginTop: 8
  },

  error: {
    borderColor: 'red'
  },

  line: {
    paddingBottom: 6,
    marginBottom: 6,
    borderColor: "#f0f0f0",
    borderBottomWidth: 1
  },

  btnText: {
    color: "#fff",
    textAlign: "center"
  },
  pageActive: {
    borderColor: '#FFA910',
  },
  buttonFLoatingContainer: {
    position: 'absolute',
    flex: 1,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: 'green',
    elevation: 10,
    borderRadius: 30
  }
});
