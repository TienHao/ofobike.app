// libary
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

// components
import { MyInputText } from 'controls/index';
import SearchLocation from '../../../SearchLocation';
import OfoDateTimePicker from '../../../../../../components/OfoDateTimePicker';

// styles
import { styles } from '../../../../style';

// utils
import { myRule } from 'utils/index';

// actions
import { getDateTime } from '../../../../../../actions';

class Receiver extends Component {
    constructor(props) {
        super(props);
    }

    handleChange = ({ name, value }) => {
        this.props.onChangeText({ name, value });
    }

    render() {
        const {
            recipientName,
            recipientPhone,
            isCheckValid,
            senderPhone,
            receiverAddress
        } = this.props;

        return (
            <View style={{ marginTop: 5 }}>
                <View
                    ref={el => this.props.positionControls["recipientName"] = el}
                    style={styles.containerInput}
                >
                    <MyInputText
                        rules={[myRule.required]}
                        checkValidForm={isCheckValid}
                        label={"Họ tên* "}
                        placeholder={"Nhập họ tên"}
                        name={"recipientName"}
                        value={recipientName}
                        onChangeText={this.handleChange}
                        onValidate={(control) => this.props.validForm.push(control)}
                    />
                </View>

                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["recipientPhone"] = el}
                >
                    <MyInputText
                        rules={[
                            myRule.required,
                            myRule.phoneNumber,
                            myRule.notEqual.bind(null, [senderPhone, "không được trùng với SĐT người giao"])
                        ]}
                        checkValidForm={isCheckValid}
                        label={"Điện thoại *"}
                        placeholder={"Nhập số điện thoại"}
                        name={"recipientPhone"}
                        value={recipientPhone}
                        keyboardType="phone-pad"
                        onChangeText={this.handleChange}
                        onValidate={(control) => this.props.validForm.push(control)}
                    />
                </View>

                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["receiverDateTime"] = el}
                >
                    <OfoDateTimePicker
                        labelDateTime={"Ngày và Giờ giao hàng*"}
                        placeholderDateTime={"Chọn ngày và giờ giao hàng"}
                        getDateTime={this.props.getDateTime}
                        checkGetDateTime={2}
                    />
                </View>

                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["senderProvince"] = el}
                >
                    <View style={{ marginBottom: 8 }}>
                        <SearchLocation
                            values={receiverAddress}
                            name="receiver"
                            placeholder="Chọn địa chỉ người nhận*"
                            checkPeople={2}
                            {...this.props}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        addressReceiver: state.mapReducer.addressReceiver
    }
}

const mapDispatchToProps = {
    getDateTime
}

export default connect(mapStateToProps, mapDispatchToProps)(Receiver);