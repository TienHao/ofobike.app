// libary
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

// components
import { MyInputText } from 'controls/index';
import SearchLocation from '../../../SearchLocation';
import OfoDateTimePicker from '../../../../../../components/OfoDateTimePicker';

// styles
import { styles } from '../../../../style';

// utils
import { myRule } from 'utils/index';

// actions
import { getDateTime } from '../../../../../../actions';

class Sender extends Component {
    constructor(props) {
        super(props);
    }

    handleChange = ({ name, value }) => {
        this.props.onChangeText({ name, value });
    }

    render() {
        const {
            isCheckValid,
            senderName,
            senderPhone,
            senderAddress
        } = this.props;



        return (
            <View>
                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["senderName"] = el}
                >
                    <MyInputText
                        rules={[myRule.required]}
                        checkValidForm={isCheckValid}
                        label={"Họ tên *"}
                        placeholder={"Nhập họ tên"}
                        name={"senderName"}
                        value={senderName}
                        onChangeText={this.handleChange}
                        onValidate={(control) => this.props.validForm.push(control)}
                    />
                </View>

                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["senderPhone"] = el}
                >
                    <MyInputText
                        rules={[myRule.required, myRule.phoneNumber]}
                        checkValidForm={isCheckValid}
                        label={"Điện thoại *"}
                        placeholder={"Nhập số điện thoại"}
                        name={"senderPhone"}
                        value={senderPhone}
                        keyboardType="phone-pad"
                        onChangeText={this.handleChange}
                        onValidate={(control) => this.props.validForm.push(control)}
                    />
                </View>
                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["receiverDateTime"] = el}
                >
                    <OfoDateTimePicker
                        labelDateTime={"Ngày và Giờ nhận hàng*"}
                        placeholderDateTime={"Chọn ngày và giờ nhận hàng"}
                        getDateTime={this.props.getDateTime}
                        checkGetDateTime={1}
                    />
                </View>

                <View
                    style={styles.containerInput}
                    ref={el => this.props.positionControls["senderProvince"] = el}
                >
                    <View style={{ marginBottom: 5 }}>
                        <SearchLocation
                            name="sender"
                            values={senderAddress}
                            checkPeople={1}
                            placeholder="Chọn địa chỉ người gửi*"
                            {...this.props}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        addressDeliver: state.mapReducer.addressDeliver
    }
}

const mapDispatchToProps = {
    getDateTime
}


export default connect(mapStateToProps, mapDispatchToProps)(Sender);