
// libary
import React from 'react';
import moment from 'moment';
import {
    findNodeHandle,
    ScrollView,
    View,
    Text
} from 'react-native';

// styles
import { styles } from "../../style";

// components 
import Sender from './components/Sender';
import Receiver from './components/Receiver';

import { compareTwoString } from '../../../../utilities';

class OrderStepOne extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ...this.getInitialState(),
            ...this.props.getDataByStep(1),
        }

        this.validForm = [];
        this.positionControls = [];
    }

    getInitialState = () => {
        return {
            isCheckValid: false,
            senderName: '',
            senderPhone: '',
            senderDate: "",
            senderTime: "",
            senderAddress: '',
            recipientName: '',
            recipientPhone: '',
            receiverAddress: '',
            receiverDate: "",
            receiverTime: ""
        }
    }

    scrollTo = ({ x, y }) => {
        this.scrollView.scrollTo({ x: 0, y, animated: true });
    }

    calculatePosition = (name) => {
        this.positionControls[name].measureLayout(
            findNodeHandle(this.scrollView),
            (x, y) => {
                this.scrollTo({ x, y });
            }
        )
    }

    handleChange = ({ name, value }) => {
        this.setState({ [name]: value });
    }

    componentDidUpdate(prevProps) {
        let checkAddressDeliver = compareTwoString(prevProps.addressDeliver, this.props.addressDeliver);
        let checkAddressReceiver = compareTwoString(prevProps.addressReceiver, this.props.addressReceiver);

        if (checkAddressDeliver !== 0) {
            this.setState({
                senderAddress: this.props.addressDeliver
            })
        }

        if (checkAddressReceiver !== 0) {
            this.setState({
                receiverAddress: this.props.addressReceiver
            })
        }
    }

    UNSAFE_componentWillReceiveProps(prevProps) {
        let checkGetDateTime = this.props.checkGetDateTime;
        let dateTime = this.props.dateTime;
        let date = moment(dateTime).format('YYYY-MM-DD');
        let time = moment(dateTime).format('hh:mm:ss');

        if (checkGetDateTime === 1) {
            this.setState({
                senderDate: date,
                senderTime: time,
            })
        }

        if (checkGetDateTime === 2) {
            this.setState({
                receiverDate: date,
                receiverTime: time,
            })
        }
    }

    checkValidate = async () => {
        try {
            this.validForm = [];
            await this._setStateAsync({ isCheckValid: true });
            this.forceUpdate();

            const data = {
                memberId: this.state.memberId,
                senderName: this.state.senderName,
                senderDate: this.state.senderDate,
                senderTime: this.state.senderTime,
                senderPhone: this.state.senderPhone,
                senderAddress: this.state.senderAddress,
                receiverName: this.state.recipientName,
                receiverDate: this.state.receiverDate,
                receiverTime: this.state.receiverTime,
                receiverPhone: this.state.recipientPhone,
                receiverAddress: this.state.receiverAddress
            }

            return {
                valid: this.validForm.every(control => {
                    if (!control.valid) {
                        this.calculatePosition(control.name);
                        return false;
                    }
                    return true;
                }),
                data: data
            }
        } catch (error) {
            return false;
        }
    }

    render() {
        const {
            isCheckValid
        } = this.state;

        return (
            <React.Fragment>
                <ScrollView
                    style={[styles.body]}
                    ref={el => this.scrollView = el}
                >
                    <View style={styles.containerForm}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{
                                borderRadius: 15,
                                borderColor: '#FCB300',
                                borderWidth: 2,
                                width: 30,
                                height: 30,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>

                                <Text>{1}</Text>
                            </View>
                            <Text style={{
                                fontSize: 22,
                                fontWeight: 'bold',
                                marginLeft: 10
                            }}>{"Người giao"}</Text>
                        </View>

                        <Sender
                            onChangeText={this.handleChange}
                            senderAddress={this.state.senderAddress}
                            senderName={this.state.senderName}
                            senderPhone={this.state.senderPhone}
                            senderTime={this.state.senderTime}
                            senderDate={this.state.senderDate}
                            isCheckValid={isCheckValid}
                            validForm={this.validForm}
                            positionControls={this.positionControls}
                            {...this.props}
                        />

                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <View style={{
                                borderRadius: 15,
                                borderColor: '#FCB300',
                                borderWidth: 2,
                                width: 30,
                                height: 30,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>

                                <Text>{2}</Text>
                            </View>
                            <Text style={{
                                fontSize: 22,
                                fontWeight: 'bold',
                                marginLeft: 10
                            }}>{"Người nhận"}</Text>
                        </View>

                        <Receiver
                            onChangeText={this.handleChange}
                            recipientName={this.state.recipientName}
                            recipientPhone={this.state.recipientPhone}
                            receiverTime={this.state.receiverTime}
                            receiverAddress={this.state.receiverAddress}
                            receiverDate={this.state.receiverDate}
                            isCheckValid={isCheckValid}
                            senderPhone={this.state.senderPhone}
                            validForm={this.validForm}
                            positionControls={this.positionControls}
                            {...this.props}
                        />
                    </View>
                </ScrollView>
            </React.Fragment>
        )
    }
}

export const OrderStep1 = OrderStepOne;


