// libary
import React, { PureComponent } from 'react';
import { ScrollView, Text, View, Image } from 'react-native';
import { styles } from "../style";

// utils
import { currencyFormat } from 'utils';

class OrderStep4 extends PureComponent {
  constructor(props) {
    super(props);

    const { serviceText } = this.props.getDataByStep(2);
    this.validForm = [];
    this.positionControls = [];

    this.state = {
      ...this.getInitialState(),
      ...this.props.getDataByStep(4),
      ...this.props.getDataByStep(3),
      serviceText
    };
  }

  getInitialState = () => {
    return {}
  }

  checkValidate = async () => {
    const data = {
      step1: { ...this.props.getDataByStep(1) },
      step2: { ...this.props.getDataByStep(2) },
      step3: { ...this.props.getDataByStep(3) }
    }

    try {
      return {
        valid: true,
        data: {
          step1: { ...this.props.getDataByStep(1) },
          step2: { ...this.props.getDataByStep(2) },
          step3: { ...this.props.getDataByStep(3) }
        }
      }
    } catch (error) {
      return false;
    }
  }

  render() {
    const { responseData } = this.state;
    const step1 = responseData[0];
    const step2 = responseData[1];
    const step3 = this.state.result;

    const { userInfo } = this.props;

    const money = userInfo.money === undefined ? 0 : userInfo.money;

    const priceDeliver = step3.fee > money ? step3.fee - money : money - step3.fee;

    return (
      <ScrollView >
        <View style={styles.containerForm}>
          <View>
            <Text style={styles.titleBlock}>
              Người giao
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Họ tên</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.sender_fullname}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Số điện thoại</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.sender_phone}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Địa chỉ</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.address_to && step1.address_from}</Text>
              </View>
            </View>
          </View>

          <View>
            <Text style={styles.titleBlock}>
              Người Nhận
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Họ tên</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.receiver_fullname}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Số điện thoại</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.receiver_phone}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Địa chỉ</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.address_from && step1.address_from}</Text>
              </View>
            </View>
          </View>

          <View>
            <Text style={styles.titleBlock}>
              Thông tin hàng
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Tên hàng</Text>
              </View>
              <View style={styles.col}>
                <Text>{step2.product_infor}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Khối lượng</Text>
              </View>
              <View style={styles.col}>
                <Text>{step2.mass} kg</Text>
              </View>
            </View>

            <View style={[styles.line]}>
              <View style={{ flex: 1 }}>
                <Text>Ảnh chụp hàng</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Image
                  style={{ height: 200 }}
                  source={{ uri: step2.image }}
                />
              </View>
            </View>
          </View>

          <View>
            <Text style={styles.titleBlock}>
              Cước phí
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Tổng tiền</Text>
              </View>
              <View style={styles.col}>
                <Text>{currencyFormat(priceDeliver)} VNĐ</Text>
              </View>
            </View>
          </View>
          <View style={{ paddingVertical: 8 }}></View>
        </View>
      </ScrollView>
    )
  }
}

export { OrderStep4 };

