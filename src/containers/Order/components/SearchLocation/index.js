// libary
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

// acitons
import { getCheckPeopleAddressDelivery } from '../../../../actions';

class SearchLocatiton extends Component {
    constructor(props) {
        super(props);
    }

    onTouchStartGetAddress = () => {
        this.props.getCheckPeopleAddressDelivery({ valueCheckPeople: this.props.checkPeople });
        this.props.navigation.navigate('Map');
    }


    render() {
        return (
            <View>
                <Text style={{ color: 'grey', fontSize: 13 }}>Địa chỉ *</Text>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        backgroundColor: 'transparante',
                        borderBottomWidth: 1,
                        borderBottomColor: 'grey'
                    }}
                    onPress={() => this.onTouchStartGetAddress()}>

                    <View style={{ flex: 1 }}>
                        <TextInput
                            multiline
                            style={{ color: 'black' }}
                            editable={false}
                            value={this.props.values}
                            placeholder={this.props.placeholder}
                        />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                        <IconAntDesign style={{ color: 'grey', marginRight: 10, fontSize: 14 }} name="caretdown" />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = {
    getCheckPeopleAddressDelivery
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchLocatiton);