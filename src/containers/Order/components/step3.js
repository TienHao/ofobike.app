// libary
import { callApi, getDeliveryFee } from 'api';
import { Loading } from 'components/Loading';
import React, { PureComponent } from 'react';
import { KeyboardAvoidingView, ScrollView, Text, View, TextInput, StyleSheet, findNodeHandle, Animated, Keyboard, UIManager, Dimensions } from 'react-native';
import { styles } from "../style";
import { GoogleMap } from './GoogleMap';

// utils
import { currencyFormat1, currencyFormat } from 'utils';

const { State: TextInputState } = TextInput;

class OrderStep3 extends PureComponent {
  constructor(props) {
    super(props);

    this.positionControls = [];

    this.state = {
      ...this.getInitialState(),
      step2: { ...this.props.getDataByStep(2) },
      shift: new Animated.Value(0)
    }
  }

  componentDidMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);

    this.initializeAsync();
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }

  async initializeAsync() {
    const authorization = this.props.accessToken;

    const step2 = this.props.getDataByStep(2);

    const api = getDeliveryFee({
      step1: this.props.getDataByStep(1),
      step2: step2,
      authorization
    });

    console.log('api', api);

    const data = {
      authorization: api.authorization,
      body: [
        {
          "address_from": api.body[0].address_from,
          "address_to": api.body[0].address_to
        },
        {
          "mass": api.body[1].mass,
          "money_fee": this.state.money_fee
        }
      ],
      method: "POST",
      url: "http://motobike.vn/api/delivery/fee"
    }

    console.log('data', data);

    const res = await callApi(data);

    console.log('res.data', res.data);

    await this.props.getFee({ currentFee: res.data.fee, distance: res.data.distance });

    if (res.success) {
      await this._setStateAsync({ result: res.data, isLoading: false });
    }
  }

  getInitialState = () => {
    return {
      isLoading: true,
      result: {},
      money_fee: 0.000,
      current_fee: 0.000
    }
  }

  checkValidate = async () => {
    try {
      return {
        valid: !this.state.isLoading,
        data: {
          step1: { ...this.props.getDataByStep(1) },
          step2: { ...this.props.getDataByStep(2) },
          ...this.state
        }
      }
    } catch (error) {
      return false;
    }
  }

  scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true });
  }

  calculatePosition = (name) => {
    this.positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this.scrollTo({ x, y })
      }
    )
  }

  onChangeTextMoneyFee = (price) => {
    this.calculatePosition("price");
  }

  handleKeyboardDidShow = (event) => {
    const { height: windowHeight } = Dimensions.get('window');
    const keyboardHeight = event.endCoordinates.height;
    const currentlyFocusedField = TextInputState.currentlyFocusedField();

    UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
      const fieldHeight = height;
      const fieldTop = pageY;

      const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);

      if (gap >= 0) {
        return;
      }
      Animated.timing(
        this.state.shift,
        {
          toValue: gap,
          duration: 100,
          useNativeDriver: true,
        }
      ).start();
    });
  }

  handleKeyboardDidHide = () => {
    Animated.timing(
      this.state.shift,
      {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }
    ).start();
  }

  render() {
    const { step2, isLoading } = this.state;
    const { fee } = this.state.result;
    const { shift } = this.state;

    if (isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Loading />
        </View>
      )
    }

    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        keyboardVerticalOffset={64}
        behavior="height"
      >
        <ScrollView
          ref={el => this._scrollView = el}
        >
          <View style={{ height: 200 }}>
            <GoogleMap
              address={step2.responseData.places}
              origin={step2.responseData.location.origin}
              destination={step2.responseData.location.destination}
            />
          </View>
          <View style={styles.containerForm}>
            <View>
              <Text style={styles.titleBlock}>
                Tiền phải trả
            </Text>
              <View style={[styles.row, { paddingBottom: 8, marginBottom: 8, borderColor: "#f0f0f0", borderBottomWidth: 1 }]}>
                <View style={styles.col}>
                  <Text>Cước phí hiện tại</Text>
                </View>
                <View style={styles.col}>
                  <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 16 }}>{currencyFormat(currencyFormat1(fee)) || 0} VNĐ</Text>
                </View>
              </View>
              {/* <View style={[{ flex: 1, flexDirection: 'row' }]}>
                <View style={[{ width: 145 }]}>
                  <Text >{`Sử dụng tiền thưởng`}</Text>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'red' }}>{`(${userInfo.money === undefined ? 0 : userInfo.money} VNĐ)`}</Text>
                </View>
                <View
                  ref={el => this.positionControls["price"] = el}
                  style={{ borderWidth: 1, flex: 1, borderColor: 'grey', paddingLeft: 20, borderRadius: 30 }}
                >
                  <TextInput
                    onBlur={() => this.calculatePosition('price')}
                    name={'price'}
                    placeholder={`${userInfo.money === undefined ? 0 : userInfo.money}`}
                    onChangeText={(text) => this.onChangeTextMoneyFee(text)}
                    multiline={true}
                    keyboardType={'numeric'}
                  />
                </View>
              </View> */}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

export { OrderStep3 };

