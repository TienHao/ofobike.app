// libary
import { MyInputText } from 'controls';
import React, { PureComponent } from 'react';
import { findNodeHandle, Image, ScrollView, Text, TouchableOpacity, View, KeyboardAvoidingView } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import { getWidthPercentage, myRule, permissionType, takePermission } from 'utils';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { styles } from "../style";

const RNFS = require('react-native-fs');

const options = {
  title: 'Chọn hình',
  chooseFromLibraryButtonTitle: "Chọn hình từ thư viện",
  takePhotoButtonTitle: "Chụp hình",
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

function getExtension(fileName) {
  let extension = fileName.substr(fileName.lastIndexOf('.') + 1);


  return extension;
}

class OrderStep2 extends PureComponent {
  constructor(props) {
    super(props)

    this.validForm = [];
    this.positionControls = [];

    this.state = {
      ...this.getInitialState(),
      ...this.props.getDataByStep(2)
    }
  }

  async componentDidMount() {
    await this._setStateAsync({ ...this.props.getDataByStep(2) });
    const { responseData } = this.state;
    await this._setStateAsync({ services: responseData.services || [] });
  }

  getInitialState = () => {
    return {
      isCheckValid: false,
      services: [],
      serviceText: "",
      errorMessagePhoto: ""
    }
  }

  checkValidImage = () => {
    if (!this.state.packageImage) {
      this.calculatePosition("image")
      return false
    }
    return true
  }

  checkValidate = async () => {
    try {
      this.validForm = [];
      await this._setStateAsync({ isCheckValid: true });
      this.forceUpdate();

      let data = this.state;

      return {
        valid: this.validForm.every(control => {
          if (!control.valid) {
            this.calculatePosition(control.name);
            return false;
          }
          return true;
        }) && this.checkValidImage(),
        data: data
      };

    } catch (error) {
      return false;
    }
  }

  handleChange = ({ name, value }) => {
    this.setState({ [name]: value });
  }

  scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true });
  }

  calculatePosition = (name) => {
    this.positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this.scrollTo({ x, y })
      }
    )
  }

  chooseImage = async () => {
    const acceptPhoto = await takePermission(permissionType.photo)
    if (!acceptPhoto) {
      this.setState({ errorMessagePhoto: "Chức năng này cần phải được cho phép truy cập ảnh" })
      return
    } else {
      this.setState({ errorMessagePhoto: "" })
    }

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let extension = getExtension(response.uri);
        console.log('extension', extension);
        ImageResizer.createResizedImage(response.uri, 400, 400, extension == 'png' ? 'PNG' : extension == 'jpg' ? 'JPEG' : 'WEBP', 72)
          .then(async (imgResize) => {
            const base64Data = await RNFS.readFile(
              imgResize.path,
              "base64"
            );
            this.setState({ packageImage: `data:image/${extension == 'jpg' ? "JPEG" : extension};base64,${base64Data}` });
          })
          .catch(err => {
            console.log(err);
          });
      }
    })
  }

  dialCall = () => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${0921041819}';
    }
    else {
      phoneNumber = 'telprompt:${0921041819}';
    }

    Linking.openURL(phoneNumber);
  }

  render() {
    const { isCheckValid } = this.state;

    return (
      <React.Fragment>
        <ScrollView
          ref={el => this._scrollView = el}
          style={styles.body}
        >
          <View style={styles.containerForm}>
            <View>
              <View>
                <Text style={[styles.titleBlock]}>
                  Thông tin Hàng hóa
              </Text>
              </View>
              <View
                style={styles.containerInput}
                ref={el => this.positionControls["packageName"] = el}
              >
                <MyInputText
                  rules={[myRule.required]}
                  checkValidForm={isCheckValid}
                  label={"Tên hàng *"}
                  placeholder={"Nhập tên hàng"}
                  name={"packageName"}
                  value={this.state.packageName}
                  onChangeText={this.handleChange}
                  onValidate={(valid) => this.validForm.push(valid)}
                />
              </View>
              <View style={styles.containerInput}>
                <Text style={styles.label} >Miêu tả sản phẩm</Text>
                <View
                  ref={el => this.positionControls["weights"] = el}
                >
                  <MyInputText
                    rules={[myRule.required]}
                    checkValidForm={isCheckValid}
                    placeholder={"Miêu tả *"}
                    name={"descriptons"}
                    value={this.state.descriptons}
                    onChangeText={this.handleChange}
                    onValidate={(valid) => this.validForm.push(valid)}
                  />
                </View>
              </View>
              <View style={styles.containerInput} >
                <Text style={styles.label}>Khối lượng (Không lớn hơn 30kg)</Text>
                <View style={styles.row}>
                  <View
                    style={styles.col}
                    ref={el => this.positionControls["weights"] = el}
                  >
                    <MyInputText
                      rules={[myRule.required]}
                      checkValidForm={isCheckValid}
                      placeholder={"Khối lượng(kg/1sp)* "}
                      name={"weights"}
                      value={this.state.weights}
                      onChangeText={this.handleChange}
                      onValidate={(valid) => this.validForm.push(valid)}
                      keyboardType="decimal-pad"
                    />
                  </View>
                </View>
              </View>

              <View style={styles.containerInput}>
                <MyInputText
                  checkValidForm={isCheckValid}
                  placeholder={"Ghi chú* "}
                  label={"Ghi chú"}
                  name={"note"}
                  value={this.state.note}
                  onChangeText={this.handleChange}
                  onValidate={(valid) => this.validForm.push(valid)}
                />
              </View>
              <View
                style={styles.containerImageStep2}
                ref={el => this.positionControls["image"] = el}
              >
                <View style={[styles.coverImageStep2, (!this.state.packageImage && isCheckValid) && styles.error]}>
                  <View
                  >
                    {
                      this.state.packageImage &&
                      <Image
                        style={{ height: getWidthPercentage(100), width: getWidthPercentage(100), resizeMode: "cover" }}
                        source={{ uri: this.state.packageImage }}
                      />
                      ||
                      <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: 'grey' }}>{"Hình ảnh của đơn hàng*"}</Text>
                      </View>
                    }
                  </View>
                </View>
                {
                  (!this.state.packageImage && isCheckValid) &&
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ color: "red" }}>Bắt buộc</Text>
                  </View>
                }
                {
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ color: "red", textAlign: "center" }}>{this.state.errorMessagePhoto}</Text>
                  </View>
                }
              </View>
            </View>
          </View>

        </ScrollView>

        <TouchableOpacity
          onPress={() => this.chooseImage()}
          activeOpacity={0.7}
          style={styles.buttonFLoatingContainer}
        >
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <IconAntDesign name='camera' style={[{ fontSize: 25, color: 'white' }]} />
          </View>
        </TouchableOpacity>

      </React.Fragment>
    )
  }
}

export { OrderStep2 };

