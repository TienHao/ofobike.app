// libary
import React, { Component } from 'react';
import { Image, Linking, StatusBar, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from './style';

class ContactScreen extends Component {
  constructor(props) {
    super(props)

    this.state = this._getInitialState();
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Liên hệ'
    };
  };

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _getInitialState = () => {
    return {}
  }

  _dialCall = () => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${0921041819}';
    }
    else {
      phoneNumber = 'telprompt:${0921041819}';
    }

    Linking.openURL(phoneNumber);
  }

  render() {

    return (
      <View style={styles.container}>
        <View
        >
          <Image
            style={{ resizeMode: "contain" }}
            source={require('../../images/logo_2.png')}
          />
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ marginBottom: 8 }}>
            <Text style={styles.content}>{"Motobike"}</Text>
          </View>

          <View style={{ marginBottom: 8 }}>
            <Text style={styles.content}>
              {"Địa chỉ: 421/27 Đồng Khởi,Khu Phố 6,Phường Tân Tiến,Thành Phố Biên Hoà,Tỉnh Đồng Nai"}
            </Text>
          </View>

        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

const ContactContainer = connect(mapStateToProps, mapDispatchToProps)(ContactScreen);
export { ContactContainer };

