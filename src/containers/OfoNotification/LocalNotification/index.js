// libary
import React, { Component, Fragment } from 'react';
import PushNotification from 'react-native-push-notification';
import { Alert } from 'react-native';

class LocalNotification extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        PushNotification.configure({
            onRegister: function (token) {
                console.log('token', token);
            },
            onNotification: function (notification) {
                if (notification.action == 'Chi Tiết') {
                    Alert.alert(notification.title, notification.bigText);
                }
            },
            senderID: '573581155022',
            permissions: {
                alert: true,
                badge: true,
                sound: true
            },
            popInitialNotification: true,
            requestPermissions: true
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { checkPusNotification, message, title } = this.props;
        if (prevProps.checkPusNotification !== checkPusNotification) {
            this.showNotification(message, title);
        }
    }

    showNotification = (message, title) => {
        PushNotification.localNotification({
            message: message,
            bigText: message,
            subText: message,
            title: title,
            actions: '["Chi Tiết", "Hủy bỏ"]'
        })
    }

    render() {
        console.log('valueCheckPushNotifsdlkf', this.props.checkPusNotification);
        return (
            <Fragment>
            </Fragment>
        )
    }
}

export default LocalNotification;