import { colors } from 'config/colors';
import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);

export const styles = StyleSheet.create({
  close: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 1
  },

  container: {
    flex: 1,
    backgroundColor: colors.bgBody
  },

  containerForm: {
    marginHorizontal: 16,
    justifyContent: 'space-around',
  },

  containerSeparate: {
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 32,
    marginHorizontal: 16
  },

  containerLoginSocial: {
    marginTop: 8,
  },

  socialItem: {
    flex: 1,
    marginVertical: 8,
    marginHorizontal: 15,
  },

  logo: {
    marginBottom: 16,
    justifyContent:"center",
    alignItems: "center",
    backgroundColor: colors.primary.default,
    borderBottomLeftRadius: 80,
    marginBottom: 16,
    padding: 16
  },

  input: {
    backgroundColor: "#fff",
    marginBottom: 8,
    borderRadius: 8
  },

  btn: {
    backgroundColor: colors.primary.default,
    height: 40,
    justifyContent: 'center',
    marginTop: 16
  },

  btnText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16
  },

  btnFacebook: {
    backgroundColor: "#3C5A99",
    justifyContent: "center",
    height: 40
  },

  btnGoogle: {
    justifyContent: "center",
    backgroundColor: "#DB4437",
    height: 40
  }
})
