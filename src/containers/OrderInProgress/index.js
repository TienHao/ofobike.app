// libary
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
    TouchableWithoutFeedback,
    ToastAndroid,
    Image
} from 'react-native';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

// styles
import { styles } from './styles';

// api
import {
    callApi,
    getApiHistories,
    getApiDeliveryChooseShipper
} from 'api';

// components
import { Loading } from 'components/Loading';

// constanst
import { SHIPPER, CUSTOMMER, CHANGE_STATUS_CANCEL_ORDER_PUSH_NOTI } from '../../constanst';

// utils
import { currencyFormat } from 'utils';



class OrderInProgress extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            historyInProgress: [],
            userInfo: null,
            isLoading: true,
            errorMessage: '',
            isDisabled: false
        }
    }

    componentDidMount() {
        this.getHistoryInProgress();

        this.focusListener = this.props.navigation.addListener('willFocus', () => {
            this.getHistoryInProgress();
        });
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.userInfo.id !== this.props.userInfo.id || prevState.historyInProgress.length !== this.state.historyInProgress.length) {
            this.getHistoryInProgress();
        }
    }

    renderNotOrderInProgress = () => {
        return (
            <View style={[styles.container, { padding: 15 }]}>
                <Text style={{ color: 'black', fontWeight: 'bold' }}>Hiện tại chưa có đơn hàng nào đang hoạt động!</Text>
                <View style={[styles.warpCard]}>
                    <View style={{ justifyContent: 'center', flex: 3, alignItems: 'center' }}>
                        <IconFontAwesome name='cart-plus' style={{ fontSize: 48, color: '#FFA910' }} />
                    </View>
                    <View style={{ flex: 7, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Order")}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Tạo đơn hàng mới</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    getHistoryInProgress = async () => {
        const { userInfo } = this.props;

        if (userInfo) {
            const authorization = this.props.accessToken;
            let res = null;
            let orderShipperChoose = null;
            const that = this;

            if (userInfo.type_id == SHIPPER) {
                res = await callApi(getApiDeliveryChooseShipper({ authorization }));
                orderShipperChoose = await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 1 }));
            } else {
                res = await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 0 }));
            }

            if (res.success) {
                const histories = res.data;
                let historyInProgress = [];

                histories.forEach(function (history) {
                    if (history.status_id === '1' || history.status_id === '2') {
                        historyInProgress.push(history);
                    }
                });

                if (orderShipperChoose !== null) {
                    orderShipperChoose.data.forEach(function (h) {
                        if (h.status_id === '2' && userInfo.type_id == SHIPPER) {
                            that.setState({
                                isDisabled: true
                            })
                        }
                    })
                }
                this.setState({ userInfo, historyInProgress: historyInProgress, isLoading: false });
            } else {
                this.setState({ errorMessage: "Xảy ra lỗi", isLoading: true });
            }

        } else {
            this.setState({ errorMessage: "Bạn chưa đăng nhập", isLoading: true });
        }
    }

    shipperChooseOrder = (item) => {
        if (this.state.isDisabled) {
            ToastAndroid.show(`Bạn có một đơn hàng đang giao \n nên không thể chọn đơn tiếp theo!`, ToastAndroid.LONG);
        } else {
            this.props.navigation.navigate("ShipperChooserOrder", { history: item });
        }
    }

    renderHistoryItem = ({ item, index }) => {
        const { userInfo } = this.props;

        return (
            <View style={{ padding: 15, marginBottom: 15, backgroundColor: 'white' }}>
                <TouchableWithoutFeedback onPress={() => this.onPressItem(item)}>
                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text>{item.code}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{currencyFormat(item.fee)} VNĐ</Text>
                            </View>
                        </View>
                        {userInfo.type_id === SHIPPER ? (
                            null
                        ) : <View style={{ marginBottom: 5 }}>
                                <Image
                                    resizeMode='stretch'
                                    source={{ uri: item.image }}
                                    style={{ height: 100, width: 120 }}
                                />
                            </View>}
                        <View style={{ marginTop: 10, marginBottom: 10, flex: 1 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View><Text>{item.address_from}</Text></View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View><Text>{item.address_to}</Text></View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>

                <View style={{ flexDirection: 'row' }}>
                    {
                        item.status_id <= 1 &&
                        <View style={{ width: 140, backgroundColor: '#FCB300', borderWidth: 0, padding: 7, elevation: 3, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    let result = userInfo.type_id == CUSTOMMER ? this.cancelOrder(item) : this.shipperChooseOrder(item);

                                    return result;
                                }}
                            >
                                <Text style={{ color: 'white' }}>
                                    {userInfo.type_id == SHIPPER ? "Chọn đơn hàng" : "Hủy đơn hàng"}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{ flex: 6, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                        <Text style={{ fontWeight: 'bold', color: 'grey' }}>{item.status_name}</Text>
                    </View>
                </View>
            </View>
        )
    }

    onPressItem = (item) => {
        this.props.navigation.navigate('HistoryDetail', { history: { ...item } });
    }

    cancelOrder = (item) => {
        this.props.navigation.navigate("CancelOrder", { history: item });
    }

    render() {
        const { isLoading, errorMessage } = this.state;
        const { userInfo } = this.props;

        return (
            <React.Fragment>
                {isLoading ?
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        {!errorMessage
                            && <Loading />
                            || <Text style={{ textAlign: "center" }}>{errorMessage}</Text>
                        }
                    </View> : (
                        <View style={styles.container}>
                            {
                                this.state.historyInProgress.length ?
                                    <FlatList
                                        initialNumToRender={10}
                                        maxToRenderPerBatch={4}
                                        updateCellsBatchingPeriod={5000}
                                        data={this.state.historyInProgress}
                                        extraData={this.state}
                                        numColumns={1}
                                        keyExtractor={(item, index) => `${item.id}-${index}`}
                                        renderItem={this.renderHistoryItem}
                                    />
                                    :
                                    userInfo.type_id === SHIPPER ? (
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>Hiện tại bạn chưa có đơn hàng mới!</Text>
                                        </View>
                                    ) : this.renderNotOrderInProgress()

                            }
                        </View>
                    )}
            </React.Fragment>
        )
    }
}

export default OrderInProgress;