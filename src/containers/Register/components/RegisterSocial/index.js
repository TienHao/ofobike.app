import React, { Component } from "react";
import { View } from "react-native";
import RegisterFacebook from './components/RegisterFacebook';
import RegisterGoogle from './components/RegisterGoogle';
import { styles } from './styles';

export default class RegisterSocial extends Component {
  render() {
    return (
      <View style={styles.container}>
        <RegisterFacebook
          {...this.props}
          caption="Đăng ký bằng facebook"
          style={styles.registerFacebook}
        />
        <RegisterGoogle
          {...this.props}
          caption="Đăng ký bằng google"
          style={styles.registerGoogle}
        />
      </View>
    );
  }
}
