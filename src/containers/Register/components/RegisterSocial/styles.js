import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 20
  },
  registerFacebook: {
    height: 40,
    marginTop: 15,
  },
  registerGoogle: {
    height: 40,
    marginTop: 20,
  }
});