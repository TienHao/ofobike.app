// libary
import React, { Component } from 'react';
import { TouchableOpacity, Text, View, TouchableHighlight } from 'react-native';
import { styles } from './styles';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from 'react-redux';

// api services
import { callApi, getApiLoginSocial } from 'api';

// actions
import { addUserInfoAction } from '../../../../../../actions';


class RegisterGoogle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userInfo: null,
            isLoginGoogle: null
        }
        this.initialize();
    }

    initialize = () => {
        this.checkTokenGoogle();
        this.registerGoogleConfig();
    }

    registerGoogleConfig = () => {
        GoogleSignin.configure({
            webClientId: "573581155022-2ce7nr4j6l9hgkurbtq0mcumrju70541.apps.googleusercontent.com",
            offlineAccess: true,
            forceConsentPrompt: true
        });
    }

    signInGoogle = async () => {
        try {
            await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
            const { user } = await GoogleSignin.signIn();

            const authorization = this.props.accessToken;

            const res = await callApi(getApiLoginSocial({
                authorization, data: {
                    email: user.email,
                    fullname: user.name
                }
            }));

            console.log('res', res);

            this.props.addUserInfoAction({ userInfo: res.user_info });
            this.props.navigation.popToTop()

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                console.log('error.code', error);
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
                console.log('error.code', error);
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                console.log('error.code', error);
            } else {
                // some other error happened
                console.log('error.code', error);
            }
        }
    }

    getCurrentUserInfo = async () => {
        const currentUser = await GoogleSignin.getCurrentUser();
        this.setState({ currentUser });
    };

    signOutGoogle = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({ userInfo: null });
        } catch (error) {
            console.error(error);
        }
    }

    checkTokenGoogle = async () => {
        const isSignedIn = await GoogleSignin.isSignedIn();
        this.setState({ isLoginGoogle: isSignedIn });
    }

    componentDidMount() {
        this.getCurrentUserInfo();
    }

    render() {
        const { isLoginGoogle } = this.state;
        return (
            <React.Fragment>
                {
                    !isLoginGoogle
                    &&
                    <TouchableOpacity onPress={this.signInGoogle} style={[styles.container, this.props.style]}>
                        <View style={styles.containerIcon}>
                            <Icon name="google" style={{ color: "white", fontSize: 35 }} />
                        </View>
                        <View style={styles.containerCaption}>
                            <Text style={{ color: 'white', fontSize: 16 }}>{this.props.caption || "Đăng ký Bằng Google"}</Text>
                        </View>
                    </TouchableOpacity>
                    ||
                    <TouchableHighlight
                        onPress={() => this.signOutGoogle()}
                    >
                        <Text style={{ textAlign: "center", color: "#fff", paddingLeft: 8 }}>Sign out</Text>
                    </TouchableHighlight>
                }
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        accessToken: state.authReducer
    }
}

const mapDispatchToProps = {
    addUserInfoAction
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterGoogle);