// libary
import React, { Component } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// styles 
import { styles } from './styles';

// api
import { callApi, getApiLoginSocial } from '../../../../../../api';

// action 
import { addUserInfoAction } from '../../../../../../actions';

class RegisterFacebook extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isRegisterFacebook: false,
            memberInfo: {}
        }
    }

    responseInfoCallback = async (error, result) => {
        if (error) {
            console.log('Error fetching data: ', error);
        } else {
            const authorization = this.props.accessToken;

            const res = await callApi(getApiLoginSocial({
                authorization, data: {
                    email: result.email,
                    fullname: result.name,
                    phone: result.phone,
                    address: ''
                }
            }));

            this.props.addUserInfoAction({ userInfo: res.user_info });
            this.props.navigation.popToTop();
        }
    }

    onPressRegisterFacebook = async () => {
        try {
            const that = this;

            await LoginManager.logInWithPermissions(["public_profile", "email"]).then(
                function (result) {
                    if (result.isCancelled) {
                        console.log("Login cancelled");
                    } else {
                        const infoRequest = new GraphRequest(
                            '/me?fields=email,name,first_name,last_name',
                            null,
                            that.responseInfoCallback
                        );
                        new GraphRequestManager().addRequest(infoRequest).start();
                    }
                }
            )


        } catch (error) {
            console.log("Login fail with error: ", { error });
        }
    }

    render() {
        return (
            <TouchableOpacity onPress={this.onPressRegisterFacebook} style={[styles.container, this.props.style]}>
                <View style={styles.containerIcon}>
                    <Icon name="facebook" style={{ color: "white", fontSize: 35 }} />
                </View>
                <View style={styles.containerCaption}>
                    <Text style={{ color: 'white', fontSize: 16 }}>{this.props.caption || "Đăng ký sử dụng facebook"}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

function mapStateToProps(state) {
    return {
        accessToken: state.authReducer
    }
}

const mapDispatchToProps = {
    addUserInfoAction
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterFacebook);