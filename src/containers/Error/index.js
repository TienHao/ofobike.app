import React, { Component } from 'react';
import { StatusBar, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getWidthPercentage } from 'utils/index';
import { styles } from './style';

class ErrorScreen extends Component {
  constructor(props) {
    super(props)

    this.state = this._getInitialState()

    const { navigation } = this.props;
    this._error = navigation.getParam('error', 'Some Things wrong!');
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _getInitialState = () => {
    return {}
  }

  render() {
    const percentLogo = getWidthPercentage(50)

    return (
      <View style={styles.container}>
        <Text>
          { this._error }
        </Text>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const ErrorContainer = connect(mapStateToProps, mapDispatchToProps)(ErrorScreen);
export { ErrorContainer };

