import { HeaderBackComp } from 'components/HeaderBack';
import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ArticleDetail } from './components';
import { styles } from './style';

class ArticleScreen extends Component {
  constructor(props) {
    super(props)

    this.state = this._getInitialState()

    const { navigation } = this.props;
    this._article = navigation.getParam('article', 'some default value');
  }

  _getInitialState = () => {
    return {
    }
  }


  componentDidMount(){
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setHidden(true)
    });

    this._navListenerBlur = this.props.navigation.addListener('willBlur', () => {
      StatusBar.setHidden(false)
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove()
    this._navListenerBlur.remove()
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.containerBack, styles.shadowStyle]}>
          <HeaderBackComp {...this.props}/>
        </View>
        <View style={styles.container}>
          {
            this._article.type === "details"
            && <ArticleDetail {...this._article}/>
          }
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const ArticleContainer = connect(mapStateToProps, mapDispatchToProps)(ArticleScreen);
export { ArticleContainer };

