// libary
import React, { Component } from 'react';
import { View } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { permissionType, takePermission } from 'utils';

// components
import SearchLocationAutoComplete from './components/SearchLocationAutoComplete';


class MapSearchLocation extends Component {
    static navigationOptions = {
        headerTransparent: true
    };

    constructor(props) {
        super(props);

        this.state = {
            permissionMessage: "",
            permissionLocation: false,
            isGetCurrentLocation: false,
            currentLocation: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.02,
                longitudeDelta: 0.02,
            }
        }
    }

    async componentDidMount() {
        const accept = await takePermission(permissionType.location);
        if (accept) {
            this.setState({ permissionLocation: true });
            this.getCurrentLocation();
        } else {
            this.setState({ permissionMessage: "Bạn cần phải cho phép truy cập vị trí để sử dụng chức năng này" });
        }
    }

    getCurrentLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                const { coords } = position;
                this.setState({
                    currentLocation: {
                        ...this.state.currentLocation,
                        latitude: coords.latitude,
                        longitude: coords.longitude
                    },
                    isGetCurrentLocation: true
                })
            },
            (error) => {
                this.setState({ permissionLocation: false });
                this.setState({ permissionMessage: "Request time out!" });
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 2 }}>
                    <SearchLocationAutoComplete valueCheckPeople={this.props.valueCheckPeople} {...this.props} />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        valueCheckPeople: state.mapReducer.valueCheckPeople
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MapSearchLocation);