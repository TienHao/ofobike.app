import AsyncStorage from '@react-native-community/async-storage';

export const KEY_AUTH = "KEY_AUTH"
export const KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
export const KEY_USER = "KEY_USER"

export const setStorageItem = async function ({key, value}) {
  try {
    await AsyncStorage.setItem(`${key}`, JSON.stringify(value))
  } catch (error) {
    console.log('Can NOT set item AsyncStorage: ', error)
  }
}

export const getStorageItem = async function(key) {
  try {
    const value = await AsyncStorage.getItem(`${key}`)
    if(value === null) {
      console.log(`Key ${key} is NOT exists!`)
      return false
    }

    return JSON.parse(value)

  } catch(error) {
    console.log('Can NOT get item AsyncStorage: ', error)
  }
}

export const removeItemValue = async function(key) {
  try {
    await AsyncStorage.removeItem(`${key}`)
    return true
  }
  catch(error) {
    console.log('Can NOT remove item AsyncStorage: ', error)
    return false
  }
}

export const cleanStorage = async function() {
  await AsyncStorage.clear()
}
