export function compareTwoString(str1 = '', str2 = '') {
    let len_str1 = str1.length;
    let len_str2 = str2.length;
    let min_len = len_str1 < len_str2 ? len_str1 : len_str2;

    for (let i = 0; i < min_len; i++) {
        if (str1[i] > str2[i]) {
            return 1; // str1 lớn hơn str2
        } else if (str1[i] < str2[i]) {
            return -1; // str1 nhỏ hơn str2
        }
    }

    if (len_str1 > min_len) {
        return 1;
    } else if (len_str2 > min_len) {
        return -1;
    }

    return 0; // bằng nhau
}

export function distance(coordinate1, coordinate2) {
    const toRadian = n => (n * Math.PI) / 180
    let lat2 = coordinate2.lat
    let lon2 = coordinate2.lon
    let lat1 = coordinate1.lat
    let lon1 = coordinate1.lon
    let R = 6371
    let x1 = lat2 - lat1
    let dLat = toRadian(x1)
    let x2 = lon2 - lon1
    let dLon = toRadian(x2)
    let a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    let d = R * c
    return d
}