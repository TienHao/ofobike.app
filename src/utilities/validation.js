export const myRule = {
  required: value => !!value || 'Bắt buộc',
  email: value => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !value ? true : (pattern.test(value));
  },
  phoneNumber: value => !value ? true : (value.length == 10),
  equal: (value1, value2) => {
    return value1[0] === value2 || (value1[1] || 'Không trùng khớp');
  },
  notEqual: (value1, value2) => {
    return value1[0] !== value2 || (value1[1] || `Không được trùng với ${value2}`);
  },
  lessOrEqual: (value1, value2) => {
    return value2 <= value1[0] || (value1[1] || 'Phải nhỏ hơn hoặc bằng');
  },
  greater: (value1, value2) => {
    return value2 > value1[0] || (value1[1] || 'Phải lớn hơn');
  },
  isNotNull: (value) => {
    return !!value
  },
  minLength: (value1, value2) => value2.length >= value1[0] || `Độ dài không đủ ${value1[0]} ký tự`
}
