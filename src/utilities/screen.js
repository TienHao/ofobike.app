import { Alert, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export const widthDevice = width
export const heightDevice = height

export  const getHeightPercentage = (percentage) => {
  const value = (percentage * heightDevice) / 100;
  return Math.round(value);
}

export  const getWidthPercentage = (percentage) => {
  const value = (percentage * widthDevice) / 100;
  return Math.round(value);
}


export const customAlert = ({title, message, handleCancel, handleOK}) => {
  Alert.alert(
    title,
    message,
    [
      {
        text: 'Cancel',
        onPress: () => handleCancel(...arguments),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => handleOK(...arguments),
        style: 'default'
      },
    ],
    {cancelable: false},
  );
}
