export const GET_SLIDE = "GET_SLIDE"
export const GET_SLIDE_SUCCESS = "GET_SLIDE_SUCCESS"
export const GET_SLIDE_FAIL = "GET_SLIDE_FAIL"

// for bindActionCreators in containers
export const getSlideAction = function() {
  return {
    type: GET_SLIDE,
    payload: {}
  }
}
