// constant
import {
    GET_CHECK_PEOPLE_ADDRESS_DELIVERY,
    GET_ADDRESS_DELIVERY
} from '../constanst';

// Actions
export function getCheckPeopleAddressDelivery(payload) {
    return {
        type: GET_CHECK_PEOPLE_ADDRESS_DELIVERY,
        payload
    }
}

export function getAddressDelivery(payload) {
    return {
        type: GET_ADDRESS_DELIVERY,
        payload
    }
}




