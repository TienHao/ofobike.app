// constants
import { GET_DATE_TIME, GET_FEE } from '../constanst';

// actions
export const getDateTime = (payload) => {
    return {
        type: GET_DATE_TIME,
        payload
    };
}

export const getFee = (payload) => {
    return {
        type: GET_FEE,
        payload
    }
}

