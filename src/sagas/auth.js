import * as actions from 'actions';
import { callApi, getApiAccessToken } from 'api';
import { put } from 'redux-saga/effects';
import { getStorageItem, KEY_ACCESS_TOKEN, KEY_USER, removeItemValue, setStorageItem } from 'utils';

export default AuthSagas = {
  getAccessToken: function*() {
    try {
      const { data } = yield callApi(getApiAccessToken(), true)

      yield put({
        type: actions.GET_ACCESS_TOKEN_SUCCESS,
        payload: { data }
      })

      yield setStorageItem({
        key: KEY_ACCESS_TOKEN,
        value: data.access_token
      })

    } catch(error) {
      yield put({
        type: actions.GET_ACCESS_TOKEN_FAIL,
        payload: { error }
      })

      console.log('Sagas:auth', error)
    }
  },

  /////////////////////////////////////////////////////////////////////////////

  getUserInfo: function* () {
    try {
      const userInfo = yield getStorageItem(KEY_USER)

      yield put({
        type: actions.GET_USER_INFO_SUCCESS,
        payload: { data: userInfo }
      })

    } catch(error) {
      yield put({
        type: actions.GET_USER_INFO_FAIL,
        payload: { error }
      })

      console.log('Sagas:getUserInfo', error)
    }
  },

  /////////////////////////////////////////////////////////////////////////////

  addUserInfo: function*({ payload }) {
    try {
      yield setStorageItem({
        key: KEY_USER,
        value: payload.data
      })

      yield put({
        type: actions.ADD_USER_INFO_SUCCESS,
        payload: { data: payload.data }
      })

    } catch(error) {
      yield put({
        type: actions.ADD_USER_INFO_FAIL,
        payload: { error }
      })

      console.log('Sagas:addUserInfo', error)
    }
  },

  /////////////////////////////////////////////////////////////////////////////

  removeUserInfo: function*() {
    try {
      yield removeItemValue(KEY_USER)

      yield put({
        type: actions.REMOVE_USER_INFO_SUCCESS,
        payload: { data: "" }
      })

    } catch(error) {
      yield put({
        type: actions.REMOVE_USER_INFO_FAIL,
        payload: { error }
      })

      console.log('Sagas:removeUserInfo', error)
    }
  }
}
