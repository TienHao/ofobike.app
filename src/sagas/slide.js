import * as actions from 'actions';
import { callApi, getApiSlides } from 'api';
import { put } from 'redux-saga/effects';

export default AuthSagas = {
  getSlide: function*() {
    try {
      const api = yield getApiSlides()
      const {success, err_code, data} = yield callApi(api)

      if(success && !err_code) {
        yield put({
          type: actions.GET_SLIDE_SUCCESS,
          payload: { data }
        })

      } else {
        yield put({
          type: actions.GET_SLIDE_FAIL,
          payload: { error: err_code }
        })
      }

    } catch(error) {
      yield put({
        type: actions.GET_SLIDE_FAIL,
        payload: { error }
      })
    }
  }
}
