import React from 'react';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { createStackNavigator } from "react-navigation-stack";
import { HeaderBackComp } from '../components/HeaderBack';
import { ArticleContainer } from '../containers/Article';
import { ChangePassContainer } from '../containers/ChangePass';
import { ContactContainer } from '../containers/Contact';
import { ErrorContainer } from '../containers/Error';
import { CancelOrderContainer } from '../containers/History/components/CancelOrder';
import { HistoryDetailContainer } from '../containers/History/components/HistoryDetail';
import { HistoryTreeContainer } from '../containers/History/components/HistoryTree';
import { MapContainer } from '../containers/Map';
import { OrderContainer } from '../containers/Order';
import { ResetPassContainer } from '../containers/ResetPass';
import { UserInfoContainer } from '../containers/UserInfo';
import OrderInProgressContainer from '../containers/OrderInProgress';
import MapGetLocationContainer from '../containers/MapGetLocation';
import { ShipperChooseOrderContainer } from '../containers/ShipperChooseOrder';
import { View, TouchableOpacity, Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import HistoryNotificationContainer from '../containers/OfoNotification/HistoryNotification';

const defaultNavigationOptionsShare = {
  headerStyle: {
    elevation: 5,
    shadowOpacity: 5,
    height: 45 + getStatusBarHeight(),
    backgroundColor: "white"
  },
  headerTitleStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: getStatusBarHeight()
  }
}

export const ContactStack = createStackNavigator(
  {
    Contact: {
      screen: ContactContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const OrderStack = createStackNavigator(
  {
    Order: {
      screen: OrderContainer,
      navigationOptions: (props) => ({
        header: () => null
      })
    },
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const OrderInProgressStack = createStackNavigator(
  {
    OrderInProgress: {
      screen: OrderInProgressContainer,
      navigationOptions: (props) => ({
        header: () => null
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const MapStack = createStackNavigator(
  {
    Map: {
      screen: MapContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const ArticleStack = createStackNavigator(
  {
    Article: {
      screen: ArticleContainer,
      navigationOptions: (props) => ({
        header: () => null
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const ErrorStack = createStackNavigator(
  {
    Error: {
      screen: ErrorContainer,
      navigationOptions: (props) => ({
        header: () => null
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)


export const UserInfoStack = createStackNavigator(
  {
    UserInfo: {
      screen: UserInfoContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const ChangePassStack = createStackNavigator(
  {
    ChangePass: {
      screen: ChangePassContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const ResetPassStack = createStackNavigator(
  {
    ResetPass: {
      screen: ResetPassContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const HistoryStack = createStackNavigator(
  {
    HistoryDetail: {
      screen: HistoryDetailContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />,
        headerRight: <TouchableOpacity
          style={{ marginTop: getStatusBarHeight(), marginRight: 10 }}
          onPress={() => props.navigation.navigate('Notification')}
        >
          <View >
            <Text>
              <MaterialIcons name='notifications' style={{ fontSize: 24, color: 'grey' }} />
            </Text>
          </View>
        </TouchableOpacity>
      })
    },
    HistoryTree: {
      screen: HistoryTreeContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    },
    CancelOrder: {
      screen: CancelOrderContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    },
    ShipperChooserOrder: {
      screen: ShipperChooseOrderContainer,
      navigationOptions: (props) => ({
        headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
      })
    }
  },
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  }
)

export const NotificationStack = createStackNavigator({
  HistoryNotification: {
    screen: HistoryNotificationContainer,
    navigationOptions: (props) => ({
      headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
    })
  },
}, {
  defaultNavigationOptions: () => ({
    ...defaultNavigationOptionsShare
  })
})

export const HistoryTreeStack = createStackNavigator({
  HistoryTree: {
    screen: HistoryTreeContainer,
    navigationOptions: (props) => ({
      headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
    })
  }
},
  {
    defaultNavigationOptions: () => ({
      ...defaultNavigationOptionsShare
    })
  })

export const MapGetLocationStack = createStackNavigator({
  MapGetLocation: {
    screen: MapGetLocationContainer,
    navigationOptions: (props) => ({
      headerLeft: <HeaderBackComp style={{ marginTop: getStatusBarHeight() }} {...props} />
    })
  }
}, {
  defaultNavigationOptions: () => ({
    ...defaultNavigationOptionsShare
  })
});
