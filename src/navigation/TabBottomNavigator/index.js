// libary
import React from "react";
import { View } from 'react-native';
import { createBottomTabNavigator } from "react-navigation-tabs";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

// containers
import { TabBarBottom } from 'components/TabBarBottom';
import { AccountContainer } from '../../containers/Account';
import { HomeContainer } from '../../containers/Home';
import { HistoryContainer } from '../../containers/History';

const sizeIcon = 24;

const RouteConfigs = {
    Home: {
        screen: HomeContainer,
        navigationOptions: ({ navigation }) => ({
            header: () => null,
            title: 'Trang chủ',
            tabBarIcon: ({ tintColor }) => {
                return (
                    <View style={{ justifyContent: "center" }}>
                        <FontAwesome5 name='home' style={{ fontSize: sizeIcon, color: tintColor }} />
                    </View>
                )
            }
        })
    },
    History: {
        screen: HistoryContainer,
        navigationOptions: ({ navigation }) => ({
            title: "Lịch sử",
            tabBarIcon: ({ tintColor }) => (
                <View style={{ justifyContent: "center" }}>
                    <IconFontAwesome name='history' style={{ fontSize: sizeIcon, color: tintColor }} />
                </View>
            )
        })
    },
    Account: {
        screen: AccountContainer,
        navigationOptions: ({ navigation }) => ({
            title: 'Tài khoản',
            tabBarIcon: ({ tintColor }) => (
                <View style={{ justifyContent: "center" }}>
                    <IconMaterial name='account' style={{ fontSize: sizeIcon, color: tintColor }} />
                </View>
            )
        })
    },
}

const BottomTabNavigatorConfig = {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarComponent: (props) => {
            const {
                navigation: { state: { index, routes } }
            } = props;

            const onTabSelect = (selectedIndex) => {
                navigation.navigate(routes[selectedIndex].routeName)
            };

            return (
                <TabBarBottom onTabSelect={onTabSelect} {...props} />
            )
        }
    }),
    tabBarOptions: {
        showLabel: true,
        activeBackgroundColor: 'white',
        inactiveBackgroundColor: 'white',
        activeTintColor: '#FCB300',
        inactiveTintColor: 'white',
        keyboardHidesTabBar: true,
        style: {
            backgroundColor: '#1F2D6A'
        }
    },
    initialRouteName: "Home"
}

const TabBottomNavigator = createBottomTabNavigator(RouteConfigs, BottomTabNavigatorConfig);

export { TabBottomNavigator };

