import { createSwitchNavigator } from "react-navigation";
import { LoginContainer } from "../containers/Login";
import { RegisterContainer } from '../containers/Register';

export const AuthSwitch = createSwitchNavigator(
  {
    Login: {
      screen: LoginContainer,
      navigationOptions: ({ navigation }) => ({
        header: () => null
      })
    },

    Register: {
      screen: RegisterContainer,
      navigationOptions: ({ navigation }) => ({
        header: () => null
      })
    }
  }
)
